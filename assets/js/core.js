jQuery(document).ready(function(){
	const _MODAL_ERROR = '<h4 class="text-center">An error occured. Check your internet connection and try again.</h4>';
	const _MODAL_LOADING = '<h4 class="text-center">Please wait...</h4>';

	function refresh_table(table_name)
	{
		var field = jQuery('[name=refresh_'+table_name+']');
		field.val(1);
		field.trigger('change');
	}

	jQuery(document).on('shown.bs.modal', '.ajax-modal', function(e){
		var modal = jQuery(this);

		jQuery.ajax({
			url      : modal.data('url'),
			type     : 'POST',
			dataType : 'JSON',
			data     : {id: e.relatedTarget.dataset.id},
			success  : function(data)
			{
				if (data.result == 'success')
				{
					modal.find('[type=submit]').prop('disabled', false);
				}

				modal.find('.modal-body').html(data.html);
			},
			error    : function()
			{
				modal.find('.modal-body').html(_MODAL_ERROR);
			}
		});
	});

	jQuery(document).on('hidden.bs.modal', '.ajax-modal', function(){
		var modal = jQuery(this);

		modal.find('.modal-body').html(_MODAL_LOADING);
		modal.find('[type=submit]').prop('disabled', true);
	});

	jQuery(document).on('submit', '.ajax-modal-form', function(e){
		e.preventDefault();

		var form  = jQuery(this);
		var btn   = form.find('[type=submit]');
		var dtxt  = btn.html();
		var modal = form.find('.modal');

		var form_data = new FormData(this);

		btn.html('Please wait...').prop('disabled', true);

		jQuery.ajax({
			url      : form.attr('action'),
			type     : 'POST',
			dataType : 'JSON',
			data     : form_data,
			cache    : false,
			contentType : false,
			processData : false,
			success  : function(data)
			{
				btn.html(dtxt).prop('disabled', false);

				if (data.result == 'success')
				{
					modal.modal('hide');
					refresh_table(form.data('refresh'));
				}
			},
			error    : function()
			{
				btn.html(dtxt).prop('disabled', false);
			}
		});
	});


	jQuery(document).on('shown.bs.modal', '.delete-modal', function(e){
		var modal = jQuery(this);

		modal.find('.btn-confirm').data('id', e.relatedTarget.dataset.id);
	});

	jQuery(document).on('click', '.delete-modal .btn-confirm', function(){
		var btn   = jQuery(this);
		var modal = btn.parents('.modal');
		var dtxt  = btn.html();

		btn.html('Deleting...').prop('disabled', true);

		jQuery.ajax({
			url      : btn.data('url'),
			type     : 'POST',
			dataType : 'JSON',
			data     : {id : btn.data('id')},
			success  : function(data)
			{
				btn.html(dtxt).prop('disabled', false);

				if (data.result == 'success')
				{
					modal.modal('hide');
					refresh_table(btn.data('refresh'));
				}
			},
			error    : function()
			{
				btn.html(dtxt).prop('disabled', false);
			}
		});
	});
		$(document).ready(function() {
	    $('#dbTab a').on('shown.bs.tab', function(e) {
	      var newt = $(e.target).attr('href');
	      var oldt = $(e.relatedTarget).attr('href');
	      $(oldt).hide();
	      //$(newt).hide().fadeIn('slow');
	      $(newt).hide().slideDown('slow');
	  });
	    $('.dropdown').on('show.bs.dropdown', function(e){
	        $(this).find('.dropdown-menu').first().stop(true, true).slideDown('fast');
	    });
	    $('.dropdown').on('hide.bs.dropdown', function(e){
	        $(this).find('.dropdown-menu').first().stop(true, true).slideUp('fast');
	    });
	    $('.hideComment').click(function(){
	        $.ajax({ url: site.base_url+'welcome/hideNotification/'+$(this).attr('id')});
	    });
	    $('.tip').tooltip();
	    $('body').on('click', '#delete', function(e) {
	        e.preventDefault();
	        $('#form_action').val($(this).attr('data-action'));
	        $('#action-form').submit();
	    });
	    $('body').on('click', '#excel', function(e) {
	        e.preventDefault();
	        $('#form_action').val($(this).attr('data-action'));
	        $('#action-form-submit').trigger('click');
	    });
	    $('body').on('click', '#pdf', function(e) {
	        e.preventDefault();
	        $('#form_action').val($(this).attr('data-action'));
	        $('#action-form-submit').trigger('click');
	    });
	});
});

$(document).on('ifChecked', '.checkth, .checkft', function(event) {
    $('.checkth, .checkft').iCheck('check');
    $('.multi-select').each(function() {
        $(this).iCheck('check');
    });
});
$(document).on('ifUnchecked', '.checkth, .checkft', function(event) {
    $('.checkth, .checkft').iCheck('uncheck');
    $('.multi-select').each(function() {
        $(this).iCheck('uncheck');
    });
});
$(document).on('ifUnchecked', '.multi-select', function(event) {
    $('.checkth, .checkft').attr('checked', false);
    $('.checkth, .checkft').iCheck('update');
});

$(document).ready(function(){
  $('input[type="checkbox"]').iCheck({
    checkboxClass: 'icheckbox_flat-green',
  });
});

$(document).ajaxComplete(function() {
    $('input[type=checkbox]').iCheck({
        checkboxClass: 'icheckbox_flat-green',
    });
});