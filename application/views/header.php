<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?><!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <title>DHVSU-COE - <?= $title ? $title : ''; ?></title>
    <link rel="shortcut icon" href="<?= site_url('assets/img/dhvtsu_favicon.png')?>"/>
    <!-- Bootstrap core JavaScript-->
    <script src="<?= site_url('vendor/jquery/jquery.min.js')?>"></script>
    <script src="<?= site_url('vendor/bootstrap/js/bootstrap.bundle.min.js')?>"></script>

    <!-- Core plugin JavaScript-->
    <script src="<?= site_url('vendor/jquery-easing/jquery.easing.min.js')?>"></script>

    <!-- Custom scripts for all pages-->
    <script src="<?= site_url('assets/js/sb-admin-2.js')?>"></script>
    <script src="<?= site_url('assets/js/core.js')?>"></script>
    <script src="<?= site_url('assets/js/custom.min.js')?>"></script>

    <!-- iCheck -->
    <link href="<?= site_url('assets/css/timeTo.css')?>" rel="stylesheet">
    <script src="<?= site_url('assets/js/icheck.js')?>"></script>

    <!-- Jquery timer -->
    <link href="<?= site_url('assets/flat/green.css')?>" rel="stylesheet">
    <script src="<?= site_url('assets/js/jquery.time-to.js')?>"></script>
    <!-- Datatables -->
    <link rel="stylesheet" type="text/css" href="<?= site_url('assets/DataTables/datatables.min.css') ?>"/>
    <script type="text/javascript" src="<?= site_url('assets/DataTables/datatables.min.js') ?>"></script>

    <link rel="stylesheet" type="text/css" href="<?= site_url('assets/css/custom.css') ?>"/>

    <!-- Custom fonts for this template-->
    <link href="<?= site_url('vendor/fontawesome-free/css/all.min.css')?>" rel="stylesheet" type="text/css">
    <!-- Custom styles for this template-->
    <link href="<?= site_url('assets/css/sb-admin-2.css')?>" rel="stylesheet">

    <!-- Pnotify -->
    <link href="<?= site_url('assets/pnotify/pnotify.custom.min.css') ?>" rel="stylesheet" type="text/css">
    <script src="<?= site_url('assets/pnotify/pnotify.custom.min.js') ?>"></script>

</head>

<body id="page-top">
<!-- Page Wrapper -->
    <div id="wrapper">
    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?= site_url('welcome'); ?>">
        <div class="sidebar-brand-icon rotate-n-15">
            <img src="<?= site_url('assets\img\coe_logo2.png')?>">
        </div>
        <div class="sidebar-brand-text mx-3">DHVSU-COE</div>
    </a>
    <!-- Divider -->
    <hr class="sidebar-divider my-0">
    <?php if($this->SuperAdmin) { ?>
    <!-- Nav Item - Dashboard -->
        <li class="nav-item welcome">
            <a class="nav-link" href="<?= site_url('welcome') ?>">
                <i class="fas fa-fw fa-tachometer-alt"></i><span>Dashboard</span>
            </a>
        </li>
        <li class="nav-item question">
            <a class="nav-link" href="<?= site_url('question')?>">
                <i class="fas fa-sitemap text-black"></i><span>Questions</span>
            </a>
        </li>
        <li class="nav-item auth">
            <a class="nav-link" href="<?= site_url('auth')?>">
                <i class="fas fa-fw fa-users text-black"></i><span>Users</span>
            </a>
        </li>
        <li class="nav-item student">
            <a class="nav-link" href="<?= site_url('student/student_results')?>">
                <i class="fas fa-fw fa-list-alt text-black"></i><span>Results</span>
            </a>
        </li>
        <li class="nav-item history">
            <a class="nav-link" href="<?= site_url('history')?>">
                <i class="fas fa-fw fa-clock text-black"></i><span>Log History</span>
            </a>
        </li>
        <li class="nav-item categories">
            <a class="nav-link" href="<?= site_url('categories')?>">
                <i class="fas fa-fw fa-list text-black"></i><span>Categories</span>
            </a>
        </li>
        <li class="nav-item settings">
            <a class="nav-link" href="<?= site_url('settings') ?>">
                <i class="fas fa-fw fa-cogs"></i><span>Settings</span>
            </a>
        </li>
        
    <?php } elseif ($this->Admin) { ?>
        <li class="nav-item welcome">
            <a class="nav-link" href="<?= site_url('welcome') ?>">
                <i class="fas fa-fw fa-tachometer-alt"></i><span>Dashboard</span>
            </a>
        </li>
        <li class="nav-item question">
            <a class="nav-link" href="<?= site_url('question')?>">
                <i class="fas fa-sitemap text-black"></i><span>Questions</span>
            </a>
        </li>
        <li class="nav-item auth">
            <a class="nav-link" href="<?= site_url('auth')?>">
                <i class="fas fa-fw fa-users text-black"></i><span>Users</span>
            </a>
        </li>
        <li class="nav-item student">
            <a class="nav-link" href="<?= site_url('student/student_results')?>">
                <i class="fas fa-fw fa-list-alt text-black"></i><span>Results</span>
            </a>
        </li>
        <li class="nav-item categories">
            <a class="nav-link" href="<?= site_url('categories')?>">
                <i class="fas fa-fw fa-list text-black"></i><span>Categories</span>
            </a>
        </li>

    <?php } else { ?>

        <li class="nav-item student">
            <a class="nav-link" href="<?= site_url('student') ?>">
                <i class="fas fa-fw fa-tachometer-alt"></i><span>Dashboard</span>
            </a>
        </li>

    <?php } ?>    
    <!-- Divider -->
    <hr class="sidebar-divider">
    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
        <!-- Main Content -->
        <div id="content">
            <!-- Topbar -->
            <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">
                <!-- Sidebar Toggle (Topbar) -->
                <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                    <i class="fa fa-bars"></i>
                </button>
                <!-- Topbar Navbar -->
                <ul class="navbar-nav ml-auto">

                    <!-- Nav Item - User Information -->
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="mr-2 d-none d-lg-inline text-gray-600 small"><b><?= $this->User; ?></b></span>
                        <!-- <img class="img-profile rounded-circle" src="https://source.unsplash.com/QAB-WJcbgJk/60x60"> -->
                        </a>
                        <!-- Dropdown - User Information -->
                        <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                            <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                                <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                Logout
                            </a>
                        </div>
                    </li>
                </ul>
            </nav>
            

          <!-- Logout Modal-->
          <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                  <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                  </button>
                </div>
                <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                <div class="modal-footer">
                  <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                  <a class="btn btn-primary" href="<?php echo site_url('auth/logout');?>">Logout</a>
                </div>
              </div>
            </div>
          </div>
        <!-- End of Topbar -->