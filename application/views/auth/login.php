<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?><!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title><?= $title; ?></title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="shortcut icon" href="<?= site_url('assets/img/dhvtsu_favicon.png')?>"/>

        <link rel="stylesheet" type="text/css" href="<?= site_url('assets/css/custom.css') ?>"/>
        <script src="<?= site_url('vendor/jquery/jquery.min.js')?>"></script>
        <script src="<?= site_url('vendor/bootstrap/js/bootstrap.bundle.min.js')?>"></script>
        <!-- Pnotify -->
        <link href="<?= site_url('assets/pnotify/pnotify.custom.min.css') ?>" rel="stylesheet" type="text/css">
        <script src="<?= site_url('assets/pnotify/pnotify.custom.min.js') ?>"></script>

        <link href="<?= site_url('assets/css/login.css')?>" rel="stylesheet">
        <link href="<?= site_url('assets/css/sb-admin-2.css')?>" rel="stylesheet">
        <link href="<?= site_url('vendor/fontawesome-free/css/all.min.css')?>" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="main">    
            <div class="container-fluid">
                <center>
                <div class="container">
                    <div class="middle">
                        <div id="login">
                            <?php if($message) { ?>
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <?= $message; ?>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                            </div>
                            <?php } ?>
                        <?php echo form_open("auth/login");?>
                            <div class="row">
                                <div class="col-md-12">
                                    <span class="fa fa-user"></span><?php echo form_input($identity);?>
                                </div>
                                <div class="col-md-12">
                                    <div id="password-input">
                                        <span class="fa fa-lock"></span><?php echo form_input($password);?>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary dark btn-sm">LOGIN</button>
                                    </div>
                                </div>
                               <!--  <div class="col-md-4">
                                    <div>
                                        <button type="button" class="btn btn-primary btn-sm" data-toggle="modal" data-target="#modal_register">REGISTER</button>
                                    </div>                    
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">
                                        <i><a href="forgot_password" class="text-gray-100 small"><?php echo lang('login_forgot_password');?></a></i>
                                    </div>
                                </div>     -->        
                                <div class="clearfix"></div>
                            <?php echo form_close();?>
                            <div class="clearfix"></div>
                            </div>
                        </div> <!-- end login -->
                        <div class="logo">                          
                			<img src="<?= site_url('assets\img\dhvsu.png')?>">
                            <img src="<?= site_url('assets\img\coe_logo.png')?>">
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                </center>
                <!-- modal register -->
                <form id="add-row-form" action="<?php echo site_url('auth/register_student');?>" method="post">
                    <div class="modal fade" id="modal_register" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="myModalLabel">Register</h4>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                </div>
                                <div class="modal-body">
                                    <div class="form-group">
                                        <input type="text" name="first_name" value="" placeholder="First Name" class="form-control" required>
                                    </div>                                    
                                    <div class="form-group">
                                        <input type="text" name="middle_name" value="" placeholder="Middle Name" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" name="last_name" value="" placeholder="Last Name" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <?php $identity_column = $this->config->item('identity', 'ion_auth');
                                        if($identity_column!=='email')
                                        {
                                            echo form_error('identity'); ?>
                                            <input type="text" name="identity" value="" class="form-control" required>
                                        <?php } ?>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" name="email" value="" placeholder="Email" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <input type="password" name="password" value="" placeholder="Password" class="form-control" required>
                                    </div>
                                    <div class="form-group">
                                        <input type="password" name="password_confirm" value="" placeholder="Confirm Password" class="form-control" required>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-success">Submit</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- end register modal -->
            </div>
        </div>
    </body>
</html>