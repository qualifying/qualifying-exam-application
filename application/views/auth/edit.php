<?php if ($user !== FALSE): ?>
    <div class="form-group">
	 	<?php echo form_input($first_name,'','class="form-control"');?>
	</div>
	<div class="form-group">
	 	<?php echo form_input($middle_name,'','class="form-control"');?>
	</div>
	<div class="form-group">
	 	<?php echo form_input($last_name,'','class="form-control"');?>
	</div>
	<div class="form-group">
		<?php
			$type[NULL] = 'Please Select User Type';
			if ($user_types !== FALSE)
			{
			  foreach ($user_types as $user_type)
			  {
			    $type[$user_type->type_id] = $user_type->name;
			  }
			}
			echo form_dropdown('user_type', $type, $user->user_type, 'class="form-control" required id="edit_user_type"');
		?>
	</div>
	<div class="form-group">
	 	<div class="col-md-2" style="float:left;padding: 7px;">
            <span>(+639)</span>
        </div>
        <div class="col-md-10" style="float:right;padding:0 0 15px;">
        	<?php echo form_input($phone,'','class="form-control"');?>
        </div>
	</div>
	<div class="form-group student">
	 	<?php echo form_input($batch,'','class="form-control"');?>
	</div>
	<div class="form-group admin">
	 	<?php echo form_input($password,'','class="form-control"');?>
	</div>	
	<div class="form-group admin">
	 	<?php echo form_input($password_confirm,'','class="form-control"');?>
	</div>
      <?php echo form_hidden('id', $user->id);?>
      <?php echo form_hidden($csrf); ?>
<?php else: ?>
	<h4 class="text-center">
		This user no longer exists.
	</h4>
<?php endif; ?>
<script type="text/javascript">
	jQuery(document).ready(function() {
		var user_type = jQuery('select#edit_user_type :selected').text();
		if (user_type == 'Admin')
		{
			jQuery('.admin').slideDown();
		    jQuery('.student').slideUp();
            jQuery('#batch').val(0);
		}
		else
		{
			jQuery('.admin').slideUp();
            jQuery('.student').slideDown();
            jQuery('#batch').val('');			
		}
	    jQuery(document).on('change', '#edit_user_type', function(){
	        if(jQuery(this).val() == 2)
	        {
	            jQuery('.admin').slideDown();
			    jQuery('.student').slideUp();
	            jQuery('#batch').val(0);	            
	        }
	        else
	        {
	            jQuery('.admin').slideUp();
	            jQuery('.student').slideDown();
	            jQuery('#batch').val('');	            
	        }
	    });
	});	
</script>