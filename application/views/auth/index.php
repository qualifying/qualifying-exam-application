<div class="container-fluid">
    <div class="card shadow mb-4">
        <?php if($this->session->flashdata('error')) { ?>
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?= $this->session->flashdata('error'); ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
        <?php } ?>

        <?php if($this->session->flashdata('success')) { ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <?= $this->session->flashdata('success'); ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
        <?php } ?>
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary">List of Users</h6>
            <div class="dropdown no-arrow">
                <ul class="nav nav-pills justify-content-end">
                    <li class="nav-item mr-2 mr-md-0">
                        <button class="btn btn-success btn-icon-split" data-toggle="modal" data-target="#modal_add">
                            <span class="icon text-white-50">
                                <i class="fas fa-plus"></i>
                            </span>
                            <span class="text">Add New</span>
                        </button>
                    </li>
                </ul>
            </div>
        </div>
        <div class="card-body">
            <div class="user_tbl">
            <?php echo form_hidden('refresh_users', 0); ?>
                <table id="user_table" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Name</th>
                            <th>Username</th>
                            <th>Type</th>
                            <th>Exam Status</th>
                            <th>Batch No.</th>
                            <th>Registered by</th>
                            <?php if($this->SuperAdmin) { ?>
                                <th>Actions</th>
                            <?php } ?>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    <!-- Modal Add User-->
    <form id="add-row-form" action="<?php echo site_url('auth/create_user');?>" method="post">
    <div class="modal fade" id="modal_add" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Add User</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <?php echo form_input($first_name,'','class="form-control" required');?>
                    </div>
                    <div class="form-group">
                        <?php echo form_input($middle_name,'','class="form-control" required');?>
                    </div>
                    <div class="form-group">
                        <?php echo form_input($last_name,'','class="form-control" required');?>
                    </div>
                    <div class="form-group">
                        <?php if($identity_column!=='email') {
                            echo form_error('identity');
                            echo form_input($identity,'','class="form-control" required');
                        } ?>
                    </div>
                    <div class="form-group">
                        <?php
                            $type[NULL] = 'Please Select User Type';
                            if ($user_types !== FALSE)
                            {
                                foreach ($user_types as $row)
                                {
                                    $type[$row->type_id] = $row->name;
                                }
                            }

                            if($this->SuperAdmin)
                            {
                                echo form_dropdown('user_type', $type, NULL, 'class="form-control" required id="user_type"');
                            }
                            else
                            {
                                echo form_dropdown('user_type', $type,3, 'class="form-control" required id="user_type"');
                            }
                        ?>
                    </div>
                    <div class="form-group admin">
                        <?php echo form_input($email,'','class="form-control" required id="student_email"');?>
                        <span id="email_result"></span>
                    </div>
                    <div class="form-group">
                        <div class="col-md-2" style="float:left;padding: 7px;">
                            <span>(+639)</span>
                        </div>
                        <div class="col-md-10" style="float:right;padding:0 0 15px;;">
                            <?php echo form_input($phone,'','class="form-control" required');?>
                        </div>
                    </div>
                    <div class="form-group student">
                        <?php echo form_input($batch,'','class="form-control"');?>
                    </div>
                    <div class="form-group admin">
                        <?php echo form_input($password,'','class="form-control" required id="student_password"');?>
                        <span id="password_result"></span>
                    </div>
                    <div class="form-group admin">
                        <?php echo form_input($password_confirm,'','class="form-control" required id="student_password_confirm"');?>
                    </div>
                </div>
                <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success" id="submit_user" disabled>Submit</button>
                
                </div>     
                </form>                       
                <!--<form action="<?php echo site_url('auth/import_user');?>" enctype="multipart/form-data" method="post">
                    <h4 class="modal-title">Import JSON FILE</h4>
                    <input style="padding: 10px;" type="file" name="importfile" id="importfile">
                    <button class="btn btn-info" type="submit" name="submit" disabled>Upload</button>
                </form> -->
                <?php
                $attrib = array('class' => 'form-horizontal', 'data-toggle' => 'validator', 'role' => 'form');
                echo form_open_multipart("auth/import_csv", $attrib)
                ?>
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="csv_file">Import CSV file</label>
                                <input type="file" data-browse-label="Browse" name="userfile" class="form-control file" data-show-upload="false" data-show-preview="false" id="csv_file" required="required"/>
                            </div>

                            <div class="form-group">
                                <?php echo form_submit('import', 'Import', 'class="btn btn-primary"'); ?>
                            </div>
                        </div>
                    </div>
                </div>
                <?= form_close(); ?>
            </div>
        </div>
    </div>

    <!-- Modal Update User-->
    <form id="update-row-form" class="ajax-modal-form" data-refresh="users" action="<?php echo site_url('auth/update');?>" method="post">
        <div class="modal fade ajax-modal" data-url="<?php echo site_url('auth/edit'); ?>" id="modal_update" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel">Update User</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <h4 class="text-center">Please wait...</h4>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success" disabled>Update</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
 
    <!-- Modal Delete User-->
    <div class="modal fade delete-modal" id="modal_delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Delete User</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <strong>Are you sure to delete this record?</strong>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                    <button type="button" class="btn btn-success btn-confirm" data-refresh="users" data-url="<?php echo site_url('auth/delete'); ?>">Yes</button>
                </div>
            </div>
        </div>
    </div>
    <script>
        function pad (str, max) {
            str = str.toString();
            return str.length < max ? pad("0" + str, max) : str;
        }

        $(document).ready(function(){
        jQuery('#importfile').change(function() {
            jQuery('[name="submit"]').prop('disabled',false);
        });
            var table = jQuery('#user_table').DataTable({
                serverSide: true,
                ajax: {
                url : '<?php echo site_url('auth/get_users'); ?>',
                type : 'POST'
            },
        lengthMenu: [50, 100],
        columns : [
            {data: 'id', className: 'text-center'},
            {data: 'name', className: 'text-center'},
            {data: 'email', className: 'text-center'},
            {data: 'type', className: 'text-center'},
            {data: 'exam_status', className: 'text-center', render: tbl_has_exam},
            {data: 'batch', className: 'text-center', render: batch},
            {data: 'created_by', className: 'text-center'},
            <?php if($this->SuperAdmin) { ?>
                {data: 'actions', className: 'text-center', orerable: false},
            <?php } ?>
        ],
            order: [[0, 'asc'],[1, 'asc']],
        });

        jQuery(document).on('change', '[name=refresh_users]', function(){
            if (jQuery(this).val() == 1) table.ajax.reload();
                jQery(this).val(0);
            });


            jQuery('#email').change(function(){  
                var email = jQuery('#email').val();  
                        if(email != '')  
                        {  
                        $.ajax({  
                            url:"<?php echo site_url('auth/check_email_avalibility'); ?>",  
                            method:"POST",  
                            data:{email:email},  
                            success:function(data){  
                                $('#email_result').html(data);  
                        }  
                    });  
                }   
            });  

        });

        jQuery(document).ready(function() {
            var user_type = jQuery('select#user_type :selected').text(),
                currentDate = new Date(),
                year = currentDate.getFullYear(),
                last_id = '<?= $last_insert_id; ?>',
                examinee_no = year+'_'+pad(last_id,5);

            if (user_type == 'Admin')
            {
                jQuery('.admin').slideDown();
                jQuery('.student').slideUp();
                jQuery('#batch').val(0);
                jQuery('[name=email]').val('');
                jQuery('[name=password]').val('');
                jQuery('[name=password_confirm]').val('');
                jQuery('#submit_user').prop('disabled',true);

                jQuery('#password').change(function(){
                    var password = jQuery('[name=password]').val().length;
                        if(password < 8)
                        {
                            jQuery('#password_result').html('<label class="text-danger"><span class="glyphicon glyphicon-remove"></span> Must be at least 8 characters in length.</label>');        
                        }
                        else
                        {
                            jQuery('#password_result').html('');
                        }
                });
            }
            else
            {
                jQuery('.admin').slideUp();
                jQuery('.student').slideDown();
                jQuery('#batch').val('');
                jQuery('[name=email]').val(examinee_no);
                jQuery('[name=password]').val(examinee_no);
                jQuery('[name=password_confirm]').val(examinee_no);
                jQuery('#submit_user').prop('disabled',false);
            }
            jQuery(document).on('change', '#user_type', function(){
                if(jQuery(this).val() == 2)
                {
                    jQuery('.admin').slideDown();
                    jQuery('.student').slideUp();
                    jQuery('#batch').val(0);                    
                    jQuery('[name=email]').val('');
                    jQuery('[name=password]').val('');
                    jQuery('[name=password_confirm]').val('');
                    jQuery('#submit_user').prop('disabled',true);

                    jQuery('#password').change(function(){
                        var password = jQuery('[name=password]').val().length;
                            if(password < 8)
                            {
                                jQuery('#password_result').html('<label class="text-danger"><span class="glyphicon glyphicon-remove"></span> Must be at least 8 characters in length.</label>');        
                            }
                            else
                            {
                                jQuery('#password_result').html('');
                            }
                    });
                }
                else
                {
                    jQuery('.admin').slideUp();
                    jQuery('.student').slideDown();
                    jQuery('#batch').val('');                    
                    jQuery('[name=email]').val(examinee_no);
                    jQuery('[name=password]').val(examinee_no);
                    jQuery('[name=password_confirm]').val(examinee_no);
                    jQuery('#submit_user').prop('disabled',false);
                }
            });
            jQuery('[name=password_confirm]').on('keyup',function(){
                if(jQuery('[name=password]').val() != jQuery('[name=password_confirm]').val())
                {
                    // add border-left-danger
                    jQuery('[name=password_confirm]').addClass('border-left-danger');
                    jQuery('#submit_user').prop('disabled',true);
                }
                else
                {
                    jQuery('[name=password_confirm]').removeClass('border-left-danger');
                    jQuery('#submit_user').prop('disabled',false);
                }
            });
            jQuery('[name=phone]').on('keyup',function(){
                if(jQuery('[name=phone]').val().length == 9)
                {
                    // add border-left-danger
                    jQuery('[name=phone]').removeClass('border-left-danger');
                }
                else
                {
                    jQuery('[name=phone]').addClass('border-left-danger');
                }
            });
        }); 
    </script>