<?php


$dataPoints = array();

foreach ($categories as $category) {
    $dataPoints[] = array('label' => $category['description'], 'y'=> $category['no_of_students'], 'students' => 'Students');
}
 
?>
<script>
window.onload = function() {
 
var date = new Date(), year = date.getFullYear();
var chart = new CanvasJS.Chart("chartContainer", {
    animationEnabled: true,
    title: {
        text: "Passing Rate"
    },
    subtitles: [{
        text: "COE " + year
    }],
    data: [{
        type: "pie",
        yValueFormatString: "#,##0",
        indexLabel: "{label} : {y} ({students})",
        dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>
    }]
});
chart.render();
 
}
</script>
        <!-- Begin Page Content -->
        <div class="container-fluid">
            <!-- Page Heading -->
            <div class="d-sm-flex align-items-center justify-content-between mb-4">
                <h1 class="h3 mb-0 text-gray-800">Dashboard</h1>
            </div>

            <div class="row">
                <div class="col-xl-12 col-lg-12 col-md-12">
                    <div class="card shadow mb-4">
                        <!-- Card Header - Dropdown -->
                        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                            <h6 class="m-0 font-weight-bold text-primary"></h6>
                        </div>
                        <div class="card-body">
                            <div id="chartContainer" style="height: 370px; width: 100%;"></div>
                            <script src="<?= site_url('assets/js/canvasjs.min.js')?>"></script>
                        </div>
                    </div>
                </div>


                <!-- Area Chart -->
                <div class="col-xl-6 col-lg-6 col-md-6">
                    <div class="card shadow mb-4">
                        <!-- Card Header - Dropdown -->
                        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                            <h6 class="m-0 font-weight-bold text-primary">UNIVERSITY VISION and MISSION</h6>
                        </div>
                        <!-- Card Body -->
                        <div class="card-body">
                            <div class="logo" style="text-align: center;">                          
                                <img src="<?= site_url('assets\img\dhvsu.png')?>">
                            </div>
                            <br/>
                            <h4>Vision</h4>
                            <br/>
                            <p>A lead university in producing quality individuals with competent capacities to generate knowledge and technology and enhance professional practices for sustainable national and global competitiveness through continuous innovation.</p>
                            <br/>
                            <h4>Mission</h4>
                            <br/>
                            <p>DHVTSU commits itself to provide an environment conducive to continuous creation of knowledge and technology towards the transformation of students into globally competitive professionals through the synergy of appropriate teaching, research, service and productivity functions.</p>
                        </div>
                    </div>
                </div>
                <!-- Area Chart -->
                <div class="col-xl-6 col-lg-6 col-md-6">
                    <div class="card shadow mb-4">
                        <!-- Card Header - Dropdown -->
                        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                            <h6 class="m-0 font-weight-bold text-primary">College of Education</h6>
                        </div>
                        <!-- Card Body -->
                        <div class="card-body">
                        <div class="logo" style="text-align: center;">
                            <img src="<?= site_url('assets\img\coe_logo.png')?>">
                        </div>
                            <br/>
                            <h4>Mission</h4>
                            <br/>
                            <p>The mission of the College of Education is to provide quality and effective instruction, personal and professional training and advancement, research and extension services to its clientele to ensure functional undergraduate teacher education.</p>
                            <br/>
                            <h4>Goals</h4>
                            <br/>
                            <p>The College of Education is committed to its task of producing highly competitive and empowered graduates who can assume the following roles:

                                <li>A dynamic and efficient conveyor of organized and systematic knowledge giving focus on the changes confronting education;</li>
                                <li>An effective promoter and facilitator of learning that will enable the learners to develop to the fullest their potentials, thus transforming the graduates to become holistically developed;</li>
                                <li>A true source of inspiration who possesses a clear understanding and appreciation of the human ideals and values that elevate the human spirit and contribute to man’s sense of satisfaction and fulfillment.</li>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>