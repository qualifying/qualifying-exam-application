<?php if ($row !== FALSE): ?>
	<?php echo form_hidden('question_id', $row->id); ?>
	<div class="form-group">
		<?php echo form_textarea($question, $row->question, 'class="form-control" placeholder="Question" required'); ?>
		<span id="e_question_result"></span>
	</div>
	<div class="form-group">
		<?php
			$cat[NULL] = 'Please Select Category';
			if ($categories !== FALSE)
			{
			  foreach ($categories as $category)
			  {
			    $cat[$category->category_id] = $category->name;
			  }
			}
			echo form_dropdown('categories', $cat, $row->category, 'class="form-control" required id="categories"');
		?>
	</div>
	<div class="form-group">
		<div class="d-flex flex-row align-items-center justify-content-between">
			<?php echo form_input($option, '', 'class="form-control" placeholder="Option"'); ?>
			<?php echo form_button('add_opt','Add', 'class="btn btn-success"'); ?>
		</div>
	</div>
	<div id="choices_edit" class="form-group">
		<?php if ($choices !== FALSE)
			{
				foreach($choices as $choice)
				{
					if($choice->option_id == $row->answer)
					{
						echo form_radio($r_choice,$choice->option_id, TRUE)
						. '<label><h5>' .form_input('e_opt'.$choice->option_id, $choice->value, 'style="border: 0; color: #858796;"') . '</h5></label></br>';
					}
					else
					{
						echo form_radio($r_choice,$choice->option_id, FALSE)
						. '<label><h5>' .form_input('e_opt'.$choice->option_id, $choice->value, 'style="border: 0; color: #858796;"') . '</h5></label></br>';
					}
				}
			}
		?>
	</div>
<?php else: ?>
	<h4 class="text-center">
		This question no longer exists.
	</h4>
<?php endif; ?>
<script type="text/javascript">
	jQuery(document).ready(function(){

		jQuery('.icheck').iCheck({
			radioClass: 'iradio_flat-green'
		});

		jQuery(document).on('click','[name=add_opt]',function(){
		    var btn = jQuery(this),
		        option = jQuery('#e_option').val(),
		        opt = jQuery('#e_option');
		        label_opt = jQuery('<label><h5>&nbsp;'+ option +'</h5></label></br>'),
		        customjs = '<script type="text/javascript">jQuery(\'.icheck\').iCheck({radioClass: \'iradio_flat-green\'})<\/script>',
		        radio_opt = jQuery('<input type="radio" class="icheck" value="' + option + '" name="radioChoice" required>'+customjs),
		        radio_opts = jQuery('<input type="hidden" value="'+option+'"name="radioOptions[]">');

		    if(option != '')
		    {
		      radio_opt.appendTo('#choices_edit');
		      radio_opts.appendTo('#choices_edit');
		      label_opt.appendTo('#choices_edit');        
		      jQuery('#e_option').val('');
		      jQuery('[type=submit]').prop('disabled',false);
		    }
		});


        jQuery('#e_question').change(function(){  
            var equestion = jQuery('#e_question').val();  
                    if(equestion != '')  
                    {  
                    $.ajax({  
                        url:"<?php echo site_url('question/check_e_question_avalibility'); ?>",  
                        method:"POST",  
                        data:{equestion:equestion},  
                        success:function(data){  
                            $('#e_question_result').html(data);  
                    }  
                });  
            }   
        });  
	})
</script>