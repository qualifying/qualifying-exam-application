<div class="container-fluid">
    <div class="card shadow mb-4">
        <?php if($this->session->flashdata('error')) { ?>
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <?= $this->session->flashdata('error'); ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
        <?php } ?>

        <?php if($this->session->flashdata('success')) { ?>
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <?= $this->session->flashdata('success'); ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
            </div>
        <?php } ?>
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary">List of Questions</h6>
            <div class="dropdown no-arrow">
                <ul class="nav nav-pills justify-content-end">
                    <?php if (!empty($categories)) { ?>
                    <nav class="navbar navbar-expand navbar-light bg-light mb-2">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Categories <span><i class="fa fa-caret-down"></i></span>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right animated--fade-in" aria-labelledby="navbarDropdown">
                                <?php
                                foreach ($categories as $category) {
                                    echo '<a class="dropdown-item" href="' . site_url('question/' . $category->category_id) . '">' . $category->name . '</a>';
                                } ?>
                                <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="<?= site_url('question'); ?>">All Questions</a>
                                </div>
                            </li>
                        </ul>
                    </nav>
                    <?php } ?>
                    <li class="nav-item mr-2 mr-md-0">
                        <button class="btn btn-success btn-icon-split" data-toggle="modal" data-target="#modal_add">
                            <span class="icon text-white-50"><i class="fas fa-plus"></i></span>
                            <span class="text">Add New</span>
                        </button>
                    </li>
                </ul>
            </div>
        </div>
        <div class="card-body">
            <div class="question_tbl">
            <?php echo form_hidden('refresh_questions', 0); ?>
                <table id="question_table" class="table table-bordered">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Question</th>
                            <th>Answer</th>
                            <th>Category</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
    <!-- Modal Add Question-->
    <form id="add-row-form" action="<?php echo site_url('question/add_question');?>" method="post">
        <div class="modal fade" id="modal_add" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel">Add Question</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <textarea id="question" name="question" class="form-control" rows="5" placeholder="Question" required></textarea>
                            <span id="question_result"></span>
                        </div>
                        <div class="form-group">

                        <?php
                            $cat[NULL] = 'Please Select Category';
                        
                            if ($categories !== FALSE)
                            {
                                foreach ($categories as $row)
                                {
                                    $cat[$row->category_id] = $row->name;
                                }
                            }

                            echo form_dropdown('categories', $cat, NULL, 'class="form-control" required id="categories"');

                        ?>

                        </div>
                        <div class="form-group">
                            <div class="d-flex flex-row align-items-center justify-content-between">
                                <input type="text" id="option" name="option" class="form-control" placeholder="Option/s"><button type="button" class="btn btn-success add_option">Add</button>
                            </div>
                        </div>
                        <div id="choices" class="form-group">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success" disabled>Submit</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
     
    <!-- Modal Update Question-->
    <form id="update-row-form" class="ajax-modal-form" data-refresh="questions" action="<?php echo site_url('question/update');?>" method="post">
        <div class="modal fade ajax-modal" data-url="<?php echo site_url('question/edit_question'); ?>" id="modal_update" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel">Update Question</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    </div>
                    <div class="modal-body">
                        <h4 class="text-center">Please wait...</h4>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-success" disabled>Update</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
     
    <!-- Modal delete Question-->
    <div class="modal fade delete-modal" id="modal_delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Delete Question</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <strong>Are you sure to delete this record?</strong>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                    <button type="button" class="btn btn-success btn-confirm" data-refresh="questions" data-url="<?php echo site_url('question/delete'); ?>">Yes</button>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function(){
            var table = jQuery('#question_table').DataTable({
                serverSide: true,
                ajax: {
                url : '<?php echo site_url('question/get_questions'.($category_id ? '/'.$category_id : '')); ?>',
                type : 'POST'
                },
            
                columns : [
                    {data: 'question_id', className: 'text-center'},
                    {data: 'question', className: 'text-center'},
                    {data: 'answer', className: 'text-center'},
                    {data: 'category', className: 'text-center'},
                    {data: 'actions', className: 'text-center', orderable: false, width : "15%"}
                ],

                order: [[0, 'asc']],
            });

        jQuery(document).on('change', '[name=refresh_questions]', function(){
            if (jQuery(this).val() == 1) table.ajax.reload();
                jQuery(this).val(0);
        });

        jQuery(document).on('click','.add_option',function(){
            var btn         = jQuery(this),
                option      = jQuery('#option').val(),
                opt         = jQuery('#option');
                label_opt   = jQuery('<label><h5>&nbsp;'+ option +'</h5></label></br>'),
                customjs    = '<script type="text/javascript">jQuery(\'.icheck\').iCheck({radioClass: \'iradio_flat-green\'})<\/script>',
                radio_opt   = jQuery('<input type="radio" class="icheck" value="' + option + '" name="radioChoice" required>'+customjs),
                radio_opts  = jQuery('<input type="hidden" value="'+option+'"name="radioOptions[]">');

            if(option != '')
            {
                radio_opt.appendTo('#choices');
                radio_opts.appendTo('#choices');
                label_opt.appendTo('#choices');

                jQuery('#option').val('');
                jQuery('[type=submit]').prop('disabled',false);
            }

        });

        jQuery('#question').change(function(){  
            var question = jQuery('#question').val();  
                    if(question != '')  
                    {  
                    $.ajax({  
                        url:"<?php echo site_url('question/check_question_avalibility'); ?>",  
                        method:"POST",  
                        data:{question:question},  
                        success:function(data){  
                            $('#question_result').html(data);  
                    }  
                });  
            }   
        });  
    });

    </script>