<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?><!DOCTYPE html>
        </div>
      </div>
      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; Qualifying Exam Application 2019</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>
<script type="text/javascript">
  jQuery(document).ready(function(){
    var controller = '<?php echo $m?>';
    jQuery('.'+controller).addClass('active');
  });
</script>
</body>

</html>