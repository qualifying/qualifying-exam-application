<div class="container-fluid">
    <div class="row">
        <div class="col-md-8 card shadow mb-4">
        <?php echo form_open('student/answers','id="answers"'); ?>

            <!-- Passing student information from previous form -->
            <?php
                foreach($questions as $key => $row)
                { 

                $count = $key + 1; ?>

                    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                        <h5 class="m-0 text-gray-700"><?php echo $count . '. ' . $row->question; ?></h5>
                    </div>
                    <div class="card-body">

                    <?php $choices = $this->student_model->get_choices($row->id);
                        foreach($choices as $choice)
                        { ?>
                            <input type="radio" class="icheck" name="choice_in_question_id_<?php echo $row->id?>" value="<?php echo $choice->option_id?>"><span class="text-gray-700"> <?php echo $choice->value ?></span><br />
                    <?php } ?>

                    </div>
            <?php } ?>
        </div>
        <div class="col-md-4 card shadow mb-4 timer border-bottom-danger border-left-danger" id="sticky_header">
            <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
                <h5 class="m-0 text-gray-700">Time Remaining :</h5>
            </div>
            <div class="card-body">
                <div id="countdown">
                </div>
            </div>
        </div>
        <div class="col-xl-8 card-footer py-4 ml-4">
            <ul class="nav nav-pills justify-content-end">
                <li class="nav-item mr-2 mr-md-0">
                    <a class="btn btn-success btn-icon-split" href="#" data-toggle="modal" data-target="#submitModal">
                        <span class="text">Submit</span>
                        <span class="icon text-white-50"><i class="fas fa-check"></i></span>
                    </a>
                </li>
            </ul>
            <!-- Submit Modal-->
            <div class="modal fade" id="submitModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Ready to Submit?</h5>
                            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">Select "Submit" below if you are ready to submit your answers.</div>
                        <div class="modal-footer">
                            <div class="dropdown no-arrow">
                                <ul class="nav nav-pills justify-content-end">
                                    <li class="nav-item mr-2 mr-md-0">
                                        <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                                        <button type="submit" class="btn btn-success btn-icon-split">
                                            <span class="text">Submit</span>
                                        </button>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php echo form_close(); ?>
    </div>

<script type="text/javascript">
    jQuery('#countdown').timeTo({
        theme: "black",
        displayCaptions: true,
        fontSize: 40,
        captionSize: 12,
        lang: 'en'
    }, <?= $remaining_time; ?>, function(){
        alert('Your Time is up!');
        jQuery('#answers').submit();
    });

    jQuery('.icheck').iCheck({radioClass: 'iradio_flat-green'});

    //sticky header
    // When the user scrolls the page, execute myFunction 
    window.onscroll = function() {myFunction()};

    // Get the header
    var sticky_header = document.getElementById("sticky_header");

    // Get the offset position of the navbar
    var sticky = sticky_header.offsetTop;

    // Add the sticky class to the header when you reach its scroll position. Remove "sticky" when you leave the scroll position
    function myFunction() {
      if (window.pageYOffset > sticky) {
        sticky_header.classList.add("sticky");
      } else {
        sticky_header.classList.remove("sticky");
      }
    }
</script>