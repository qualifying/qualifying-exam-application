<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?><!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Results PDF</title>
    <link href="<?=site_url('assets/css/bootstrap.min.css')?>" rel="stylesheet">
    <link href="<?=site_url('assets/css/pdf.css')?>" rel="stylesheet">
</head>

<body>
<div id="wrap">
    <div class="row">                      
        <div class="col-lg-3 col-xs-3 text-center">
            <div class="pull-left"><img style="width: 50%;" src="<?= site_url('assets\img\dhvsu.png')?>"></div>
        </div>
        <div class="col-lg-4 col-xs-4 text-center">
            <h2>Republic of the Philippines</h2>
            <h3>Don Honorio Ventura State University</h3>
            <h3>Bacolor, Pampanga</h3>
            <h2>College of Education</h2>
            <h3>Qualifying Examination Result</h3>
        </div>
        <div class="col-lg-3 col-xs-3 text-center">
            <div class="pull-right"><img style="width: 50%;" src="<?= site_url('assets\img\coe_logo.png')?>"></div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12" style="margin-top: 15px;">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th style="width: 15%;">#</th>
                            <th>Name</th>
                            <th>Batch No.</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $student_no = 1; ?>  
                        <?php foreach ($student_passed as $student) { ?>
                        <tr> 
                            <td><?= $student_no; ?></td>
                            <td><?= $student->first_name . ' ' . $student->middle_name . ' ' . $student->last_name ?></td>
                            <?php $student_no++; ?>
                            <td class="text-center" style="width:10%"><?= $student->batch ?></td>
                        <?php } ?>
                    </tbody>
                </table>
            </div>
        </div>
        <div class="clearfix"></div>
            <div class="col-xs-4 pull-left">
                <p style="height: 30px;">Prepared by:</p>
                <?= $user->first_name .' '.$user->middle_name .' '. $user->last_name; ?>
                <p style="height: 30px;"></p>
                <p style="height: 30px;">Date:</p>
                <?= $date; ?>
            </div>
        <div class="clearfix"></div>
    </div>
</body>
</html>
