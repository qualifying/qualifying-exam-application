  <script>
    $(document).ready(function(){
      var table = jQuery('#results_table').DataTable({
        serverSide: true,
        ajax: {
          url : '<?php echo site_url('student/get_student_results'); ?>',
          type : 'POST'
        },
        lengthMenu: [50, 100],
        columns : [
          {data: 'id', className: 'text-center', render : tbl_checkbox, orderable:false},
          {data: 'name', className: 'text-center'},
          {data: 'start_date', className: 'text-center'},
          {data: 'phone', className: 'text-center',  render : tbl_phone},
          {data: 'result_total', className: 'text-center'},
          {data: 'average', className: 'text-center'},
          {data: 'course_passed', className: 'text-center'},
          {data: 'result', className: 'text-center', render : result},
          {data: 'batch', className: 'text-center'},
        ],
        order: [[1, 'asc']],
      });

      jQuery(document).on('change', '[name=refresh_results]', function(){
        if (jQuery(this).val() == 1) table.ajax.reload();
        jQuery(this).val(0);
      });
    });
</script>
<?php echo form_open('student/bulk_actions', 'id="action-form"'); ?>
<div class="container-fluid">
  <div class="card shadow mb-4">
    <?php if($this->session->flashdata('error')) { ?>
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <?= $this->session->flashdata('error'); ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
        </div>
    <?php } ?>

    <?php if($this->session->flashdata('success')) { ?>
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <?= $this->session->flashdata('success'); ?>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
        </div>
    <?php } ?>
    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
      <h6 class="m-0 font-weight-bold text-primary">Student Results</h6>  
    
      <ul class="nav nav-pills justify-content-end">
            <li class="nav-item mr-2 mr-md-0">
                <a data-toggle="dropdown" href="#">
                    <i class="icon fa fa-tasks tip" data-placement="left" title="Bulk Actions"></i>
                </a>
                <ul class="dropdown-menu pull-right tasks-menus" role="menu" aria-labelledby="dLabel">
                    <li>
                        <a href="#" id="excel" data-action="export_excel">
                            <i class="fa fa-file-excel"></i> Export to Excel (PASSED)
                        </a>
                    </li>
                    <li>
                        <a href="#" id="excel" data-action="export_excel_">
                            <i class="fa fa-file-excel"></i> Export to Excel (SELECTED)
                        </a>
                    </li>
                    <li>
                        <a href="#" id="pdf" data-action="generate_pdf">
                            <i class="fa fa-file-pdf"></i> Generate PDF
                        </a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
    <div class="card-body">
      <div class="user_tbl">
        <?php echo form_hidden('refresh_results', 0); ?>
        <table id="results_table" class="table table-bordered">
            <thead>
                <tr>
                  <th style="min-width:30px; width: 30px; text-align: center;">
                      <input class="checkth" type="checkbox" name="check"/>
                  </th>
                  <th>Name</th>
                  <th>Exam Date</th>
                  <th>Phone</th>
                  <th>Result Total</th>
                  <th>Average</th>
                  <th>Course Passed</th>
                  <th>Result</th>
                  <th>Batch No.</th>
                </tr>
            </thead>
        </table>
    </div>
  </div>
<div style="display: none;">
    <input type="hidden" name="form_action" value="" id="form_action"/>
        <?= form_submit('performAction', 'performAction', 'id="action-form-submit"') ?>
    </div>
<?= form_close() ?>
</div>