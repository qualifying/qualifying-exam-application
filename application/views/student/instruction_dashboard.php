<div class="container-fluid">
    <?php echo form_open('student/exam'); ?>
    <div class="card shadow mb-4">
        <?php if($this->student_model->has_exam($this->session->userdata('user_id'))) { ?>
            <div class="card-header border-left-danger py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-gray-700">Note</h6>
            </div>
        <?php }
            else { ?>
            <div class="card-header border-left-info py-3 d-flex flex-row align-items-center justify-content-between">
                    <h6 class="m-0 font-weight-bold text-gray-700">EXAMINATION GUIDELINES</h6>
            </div>
        <?php } ?>
        <div class="card-body">
            <?php if($this->student_model->has_exam($this->session->userdata('user_id'))) { ?>
                    <i class="m-0 text-gray-700">Congratulations! You have already taken the exam.</i>
                    <br />
                    <br />
                    <b class="text-info">NOTE : Please wait for Admin to release your results.</b>
            <?php }
                else { ?>
                    <i class="m-0 text-gray-700"><b>Please Read Carefully. You will not receive any special consideration if you misread or fail to follow the instructions.</b>
                    </i><br><br>

                    <b>1. </b> No personal items, including <b>cellular phones</b>, <b>hand-held computers</b>, <b>bags</b>, <b>notes</b>, <b>books</b> are not allowed in the testing room.<br><br>
                    <b>2.</b>
                    If you experience hardware or software  <b>problems or distractions</b> that affect your ability to take the exam, notify the administrator immediately by  <b>raising your hand</b>. If you have other questions or concerns, raise your hand and the administrator will assist you as long as other examinees are not disturbed.<br><br>

                   <b> 3. </b>
                    The administrator <b>cannot</b> answer your questions related to the exam content.<br><br>

                   <b> 4. </b>
                    Clicking <b>back</b> , <b>refresh</b> , <b>exit button</b> will result to an invalid examination.<br><br>

                   <b> 5.</b>
                    If observed <b>violating</b>  the above rules, if you <b>tamper</b>  with the computer or if you are suspected of <b>cheating</b>, appropriate action will be taken. This may include invalidation of your exam results.<br><br>

                    <b>6.</b>
                    You must <b>leave</b> the testing room at once as soon as your examination has ended.<br><br>

                   <b> 7. </b>
                    To ensure a high level of security throughout your testing experience, you will be <b>monitored</b> at all times.<br><br>

                    <b>8.</b>
                    You have <b>1 hour</b> to answer all the questions.<br><br>

                   <b> 9.</b>
                    Once you click the <b>submit button</b> make sure that you already answered all the questions.<br><br>

                    <b>10.</b>
                    Answer your questions <b>carefully</b>, once you start the exam you must finish it because it will <b>automatically</b> submit your answers when the time expired.<br><br>

                   <i class="m-0 text-gray-700"><b> Please read the questions carefully before answering. GOODLUCK!</i></b>





            <?php } ?>
        </div>
        <div class="card-footer py-4">
            <div class="dropdown no-arrow">
                <ul class="nav nav-pills justify-content-end">
                    <li class="nav-item mr-2 mr-md-0">
                        <button id="start" class="btn btn-primary btn-icon-split">
                            <span class="text">Start Exam</span>
                            <span class="icon text-white-50"><i class="fas fa-hourglass"></i></span>
                        </button>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <?php echo form_close(); ?>
<script type="text/javascript">
    jQuery(document).ready(function(){
        if('<?= $this->student_model->has_exam($this->session->userdata('user_id'))?>')
        {
            jQuery('#start').attr('disabled','disabled');
        }
        else
        {
            jQuery('#start').removeAttr('disabled');
        }
    });
</script>