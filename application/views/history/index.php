<div class="container-fluid">
  <div class="card shadow mb-4">
    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
      <h6 class="m-0 font-weight-bold text-primary">Log History</h6>
    </div>
    <div class="card-body">
      <div class="history_tbl">
        <?php echo form_hidden('refresh_history', 0); ?>
        <table id="history_table" class="table table-bordered">
            <thead>
                <tr>
                  <th>User</th>
                  <th>Activity Date</th>
                  <th>Activity</th>
                </tr>
            </thead>
        </table>
      </div>
    </div>
  </div>
<script>
    $(document).ready(function(){
      var table = jQuery('#history_table').DataTable({
        serverSide: true,
        ajax: {
          url : '<?php echo site_url('history/get_history'); ?>',
          type : 'POST'
        },
        columns : [
          {data: 'user_fullname', className: 'text-center'},
          {data: 'activity_date', className: 'text-center'},
          {data: 'activity', className: 'text-center'}
        ],
        order: [[1, 'desc']],
      });

      jQuery(document).on('change', '[name=refresh_history]', function(){
        if (jQuery(this).val() == 1) table.ajax.reload();
        jQuery(this).val(0);
      });
    });
</script>