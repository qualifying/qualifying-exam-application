<?php if ($row !== FALSE): ?>
	<?php echo form_hidden('category_id', $row->category_id); ?>
	<div class="form-group">		
		<?php echo form_input($name, $row->name, 'class="form-control" placeholder="Name"'); ?>
	</div>
	<div class="form-group">
		<?php echo form_textarea($description, $row->description, 'class="form-control" placeholder="Description" required'); ?>
	</div>
<?php else: ?>
	<h4 class="text-center">
		This category no longer exists.
	</h4>
<?php endif; ?>