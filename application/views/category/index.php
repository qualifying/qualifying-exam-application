<div class="container-fluid">
  <div class="card shadow mb-4">
    <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
      <h6 class="m-0 font-weight-bold text-primary">List of Question's Categories</h6>
      <div class="dropdown no-arrow">
        <ul class="nav nav-pills justify-content-end">
          <li class="nav-item mr-2 mr-md-0">
            <button class="btn btn-success btn-icon-split" data-toggle="modal" data-target="#modal_add">
              <span class="icon text-white-50">
                <i class="fas fa-plus"></i>
              </span>
              <span class="text">Add New</span>
            </button>
          </li>
        </ul>
      </div>
    </div>
    <div class="card-body">
      <div class="category_tbl">
        <?php echo form_hidden('refresh_categories', 0); ?>
        <table id="category_table" class="table table-bordered">
            <thead>
                <tr>
                  <th>ID</th>
                  <th>Name</th>
                  <th>Description</th>
                  <th>Actions</th>
                </tr>
            </thead>
        </table>
      </div>
    </div>
  </div>
<!-- Modal Add Category-->
<form id="add-row-form" action="<?php echo site_url('categories/add_category');?>" method="post">
    <div class="modal fade" id="modal_add" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Add Category</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                      <input name="name" id="name" class="form-control" placeholder="Name" required></textarea>
                    </div>
                    <div class="form-group">
                      <textarea name="description" class="form-control" rows="5" placeholder="Description" required></textarea>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success">Submit</button>
                </div>
            </div>
        </div>
    </div>
</form>
 
<!-- Modal Update Category-->
<form id="update-row-form" class="ajax-modal-form" data-refresh="categories" action="<?php echo site_url('categories/update');?>" method="post">
    <div class="modal fade ajax-modal" data-url="<?php echo site_url('categories/edit_category'); ?>" id="modal_update" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                  <h4 class="modal-title" id="myModalLabel">Update Category</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <h4 class="text-center">Please wait...</h4>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success" disabled>Update</button>
                </div>
            </div>
        </div>
    </div>
</form>
 
<!-- Modal delete Question-->
<div class="modal fade delete-modal" id="modal_delete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel">Delete Category</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <strong>Are you sure to delete this record?</strong>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">No</button>
                <button type="button" class="btn btn-success btn-confirm" data-refresh="categories" data-url="<?php echo site_url('categories/delete'); ?>">Yes</button>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
      var table = jQuery('#category_table').DataTable({
        serverSide: true,
        ajax: {
          url : '<?php echo site_url('categories/get_categories'); ?>',
          type : 'POST'
        },
        columns : [
          {data: 'id', className: 'text-center'},
          {data: 'name', className: 'text-center'},
          {data: 'description', className: 'text-center'},
          {data: 'actions', className: 'text-center', orderable: false, width : "15%"}
        ],
        order: [[0, 'asc']],
      });

      jQuery(document).on('change', '[name=refresh_categories]', function(){
        if (jQuery(this).val() == 1) table.ajax.reload();
        jQuery(this).val(0);
      });
    });
</script>