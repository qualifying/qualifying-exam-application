<div class="container-fluid">
	<div class="card shadow mb-4 col-md-3">
		<div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
			<h6 class="m-0 font-weight-bold text-primary">Exam Duration ( minutes )</h6>
		</div>
    	<div class="card-body">
    		<?php echo form_open('settings/adjust_duration'); ?>
    		<?php echo form_input($time,$current_exam_duration)?>
    	</div>
    	<div class="card-footer py-4">
            <div class="dropdown no-arrow">
                <ul class="nav nav-pills justify-content-end">
                    <li class="nav-item mr-2 mr-md-0">
                        <button type="submit" class="btn btn-success btn-icon-split">                        
                            <span class="text">Save</span>
                            <span class="icon text-white-50"><i class="fas fa-check"></i></span>
                        </button>
                    </li>
                </ul>
            </div>
    		<?php echo form_close(); ?>
        </div>
    </div>
</div>
<div class="container-fluid">
    <div class="card shadow mb-4 col-md-3">
        <div class="card-header py-3 d-flex flex-row align-items-center justify-content-between">
            <h6 class="m-0 font-weight-bold text-primary">Student Results</h6>
        </div>
        <div class="card-body">
            <?php echo form_open('settings/export'); ?>
            <div class="dropdown no-arrow">
                <ul class="nav nav-pills justify-content-end">
                    <li class="nav-item mr-2 mr-md-0">
                        <button type="submit" class="btn btn-secondary btn-icon-split">                        
                            <span class="text">Export CSV</span>
                            <span class="icon text-white-50"><i class="fas fa-arrow-right"></i></span>
                        </button>
                    </li>
                </ul>
            </div>
            <?php echo form_close(); ?>
        </div>