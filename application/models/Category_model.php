<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Category_model extends CI_Model
{
    //insert data method
    public function add_category($category_data)
    {        
        $result = $this->db->insert('categories', $category_data);
    
        return $result;
    }

    public function get_category($id)
    {
        $query = $this->db->where('category_id', $id)
                          ->limit(1)
                          ->get('categories');

        return ($query->num_rows() > 0) ? $query->row() : FALSE;
    }

    public function update_category($id, array $category_data)
    {
        if ($this->db->update('categories', $category_data, ['category_id' => $id]))
        {
            $this->ion_auth->set_message('Category successfully updated.');
            return TRUE;
        }
        else
        {
            $this->ion_auth->set_error('Category update failed.');
            return FALSE;
        }
    }

    public function delete_category($id)
    {
        $query = $this->db->where('category_id', $id)
                          ->delete('categories');                              
        return TRUE;            
    }

}