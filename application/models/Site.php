<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Site extends CI_Model
{

    public function __construct() {
        parent::__construct();
    }

    public function get_user($id = NULL)
    {
        if (!$id)
        {
            $id = $this->session->userdata('user_id');
        }

        $q = $this->db->get_where('users', array('id' => $id), 1);

        if ($q->num_rows() > 0)
        {
            return $q->row();
        }
        return FALSE;
    }

    public function get_user_type($user_id = false)
    {
	    if (!$user_id)
	    {
	        $user_id = $this->session->userdata('user_id');
	    }

	    $type_id = $this->get_user_type_id($user_id);

	    $q = $this->db->get_where('user_types', array('type_id' => $type_id), 1);

	    if ($q->num_rows() > 0)
	    {
	        return $q->row();
	    }

	    return FALSE;
    }

    public function get_user_type_id($user_id = false)
    {
        $user = $this->get_user($user_id);
        return $user->user_type;
    }

    public function get_user_name($user_id)
    {
    	$user = $this->db->get_where('users',array('id'=>$user_id));
    	return ($user->num_rows() > 0) ? $user->row()->first_name . ' ' . $user->row()->last_name : FALSE;
    }

    public function get_exam_duration()
    {
        $q = $this->db->get('settings');

        if ($q->num_rows() > 0)
        {
            return $q->row();
        }

        return FALSE;
    }

    public function adjust_duration($current_exam_duration, array $time)
    {
        if ($this->db->update('settings', $time, ['exam_duration' => $current_exam_duration]))
        {
            $this->ion_auth->set_message('Exam duration successfully updated.');
            return TRUE;
        }
        else
        {
            $this->ion_auth->set_error('Exam duration update failed.');
            return FALSE;
        }
    }

    function fetch_data()
    {
        return $this->db->get('student_results');
    }

    public function delete_student_result($id)
    {
        $query = $this->db->where('student_id', $id)
                          ->delete('student_results');                              
        return TRUE;
    }

    public function get_categories()
    {
        $query = $this->db->select('categories.category_id, categories.name, categories.description, COUNT(course_passed.student_id) as no_of_students')
                          ->join('course_passed','course_passed.category_id=categories.category_id', 'left')
                          ->join('student_results','student_results.student_id=course_passed.student_id', 'left')
                          ->group_by('categories.category_id')
                          ->get('categories');

        return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
    }
}