<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Question_model extends CI_Model
{
    //insert data method
    public function add_question($question_data)
    {        
        $result = $this->db->insert('questions', $question_data);
    
        return $result;
    }

    public function get_question($id)
    {
        $query = $this->db->where('id', $id)
                          ->limit(1)
                          ->get('questions');

        return ($query->num_rows() > 0) ? $query->row() : FALSE;
    }

    public function get_categories()
    {
        $query = $this->db->get('categories');

        if ($query->num_rows() > 0) {
            foreach (($query->result()) as $row)
            {
                $data[] = $row;
            }

            return $data;
        }
    }

    public function update_question($id, array $question_data)
    {
        if ($this->db->update('questions', $question_data, ['id' => $id]))
        {
            $this->ion_auth->set_message('Question successfully updated.');
            return TRUE;
        }
        else
        {
            $this->ion_auth->set_error('Question update failed.');
            return FALSE;
        }
    }

    public function delete_question($id)
    {
        $query = $this->db->where('id', $id)
                          ->delete('questions');                              
        return TRUE;            
    }

    public function get_options($id)
    {
        $query = $this->db->where('question_id', $id)
                          ->get('options');

        return ($query->num_rows() > 0) ? $query->result() : FALSE;
    }

    public function add_option($additional_option_data)
    {        
        $result = $this->db->insert('options', $additional_option_data);
    
        return $result;
    }

    public function update_option($option_id, array $edit_option_data)
    {
        if ($this->db->update('options', $edit_option_data, ['option_id' => $option_id]))
        {
            $this->ion_auth->set_message('Option successfully updated.');
            return TRUE;
        }
        else
        {
            $this->ion_auth->set_error('Option update failed.');
            return FALSE;
        }
    }
    function is_question_available($question)  
    {  
        $this->db->where('question', $question);  
        $query = $this->db->get("questions");

        if($query->num_rows() > 0)  
        {  
            return true;  
        }  
        else  
        {  
            return false;  
        }  
    }  
}