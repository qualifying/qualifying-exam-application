<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Student_model extends CI_Model
{
    public function get_questions()
    {
        $q = $this->db->get('questions');

        return $q->num_rows() > 0 ? $q->result() : FALSE ;
    }

    public function get_categories()
    {
        $q = $this->db->get('categories');

        return $q->num_rows() > 0 ? $q->result() : FALSE ;
    }

    public function get_choices($question_id)
    {
        $q = $this->db->get_where('options',array('question_id'=>$question_id));

        return $q->num_rows() > 0 ? $q->result() : FALSE ;
    }

    public function has_exam($student_id)
    {
        $q = $this->db->get_where('users',array('id'=>$student_id));

        if ($q->num_rows() > 0)
        {
            if ($q->row()->has_exam == 1)
            {
                return TRUE;
            }
            else
            {
                return FALSE;
            }
        }
        else
        {
            return FALSE;
        }
    }

    public function insert(array $student_results)
    {
        $result = $this->db->insert('student_results',$student_results);

        return $result;
    }

    public function get_student_data($student_id)
    {
        $q = $this->db->get_where('users', array('id'=>$student_id));

        return $q->num_rows() > 0 ? $q->row() : FALSE;
    }

    public function get_student_results($student_id)
    {
        $q = $this->db->get_where('student_results', array('student_id'=>$student_id));

        return $q->num_rows() > 0 ? $q->row() : FALSE;
    }

    public function get_student_exam_end($student_id)
    {
        $q = $this->db->get_where('users', array('id'=>$student_id));

        return $q->num_rows() > 0 ? $q->row()->end_date : FALSE;
    }

    public function student_exam_end($student_id, array $datetime)
    {
        $q = $this->db->update('users', $datetime, ['id' => $student_id]);
    }

    public function insert_has_exam($student_id, array $data)
    {
        if ($this->db->update('users', $data, ['id' => $student_id]))
        {
            $this->ion_auth->set_message('Exam information successfully updated.');
            return TRUE;
        }
        else
        {
            $this->ion_auth->set_error('Update failed.');
            return FALSE;
        }
    }

    public function get_number_of_categories()
    {
        $q = $this->db->get('categories');

        return $q->num_rows() > 0 ? $q->num_rows() : FALSE;

    }

    public function get_number_of_questions()
    {
        $q = $this->db->get('questions');

        return $q->num_rows() > 0 ? $q->num_rows() : FALSE;

    }

    public function get_number_of_question_in($category_id)
    {
        $q = $this->db->get_where('questions',array('category'=>$category_id));

        return $q->num_rows() > 0 ? $q->num_rows() : FALSE;

    }

    public function get_students_passed()
    {
        $q = $this->db->select('users.first_name, users.middle_name, users.last_name, users.batch')
                      ->join('users','users.id=student_results.student_id','left')
                      ->where('student_results.passed = 1')
                      ->get('student_results');

        return $q->num_rows() > 0 ? $q->result() : FALSE;
    }

    public function insert_course_passed(array $course_passed_)
    {
        $result = $this->db->insert('course_passed',$course_passed_);

        return $result;
    }
}