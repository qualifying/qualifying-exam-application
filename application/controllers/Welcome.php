<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct()
    {
        parent::__construct();
        // $this->load->model('department_model');
    }
	function index()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		else
		{
			if($this->ion_auth->in_type('Admin') || $this->ion_auth->in_type('SuperAdmin'))
			{
				$this->data['categories'] = $this->site->get_categories();

				$this->load->view('header',$this->data);
				$this->load->view('dashboard',$this->data);
				$this->load->view('footer',$this->data);
			}
			else
			{
				redirect('/student','refresh');
			}			
		}
	}
}
