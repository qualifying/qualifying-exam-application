<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Settings extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct()
    {
        parent::__construct();
    }
	function index()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('settings', 'refresh');
		}
		else
		{
			$this->data['time'] = array(
			  'name' => 'time',
			  'id'   => 'time',
			  'class'=> 'form-control',
			  'type' => 'number'
			);
			$this->data['current_exam_duration'] = $this->site->get_exam_duration()->exam_duration;
			$this->load->view('header',$this->data);
			$this->load->view('settings/index',$this->data);
			$this->load->view('footer',$this->data);			
		}
	}

	function adjust_duration()
	{
		if(!$this->ion_auth->logged_in())
		{
			redirect('auth/login', 'refresh');
		}
		else
		{
			$time = array(
				'exam_duration' => $this->input->post('time')
			);
			$current_exam_duration = $this->data['current_exam_duration'] = $this->site->get_exam_duration()->exam_duration;

			if($this->site->adjust_duration($current_exam_duration,$time))
			{
				$this->data['time'] = array(
				  'name' => 'time',
				  'id'   => 'time',
				  'class'=> 'form-control',
				  'type' => 'number'
				);
				$history_data = array(
					'user_id' => $this->session->userdata('user_id'),
					'date' => date('Y-m-d H:i:s'),
					'activity' => 'Adjust Exam Duration',
				);   
				$result = $this->db->insert('log_history', $history_data);

				$this->data['current_exam_duration'] = $this->site->get_exam_duration()->exam_duration;

				$this->load->view('header',$this->data);
				$this->load->view('settings/index',$this->data);
				$this->load->view('footer',$this->data);
			}
			else
			{
				redirect('settings','refresh');
			}
		}
	}

	function export()
	{
        $history_data = array(
            'user_id' => $this->session->userdata('user_id'),
            'date' => date('Y-m-d H:i:s'),
            'activity' => 'Export CSV',
        );   
        $result = $this->db->insert('log_history', $history_data);
		$file_name = 'student_results_on_'.date('Y_m_d').'.csv'; 
		header("Content-Description: File Transfer"); 
		header("Content-Disposition: attachment; filename=$file_name"); 
		header("Content-Type: application/csv;");

		// get data 
		$student_data = $this->site->fetch_data();

		// file creation 
		$file = fopen('php://output', 'w');

		$header = array("ID","Result","Average","Course Passed","Passed"); 
		fputcsv($file, $header);
		foreach ($student_data->result_array() as $key => $value)
		{ 
		fputcsv($file, $value); 
		}
		fclose($file); 
		exit; 
	}
}
