<?php defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Auth
 * @property Ion_auth|Ion_auth_model $ion_auth        The ION Auth spark
 * @property CI_Form_validation      $form_validation The form validation library
 */
class Auth extends MY_Controller
{
	public $data = [];

	public function __construct()
	{
		parent::__construct();

		$this->form_validation->set_error_delimiters($this->config->item('error_start_delimiter', 'ion_auth'), $this->config->item('error_end_delimiter', 'ion_auth'));

		$this->lang->load('auth');

		$this->digital_upload_path = 'files/';
		$this->allowed_file_size = '1024';
	}

	// Users Datatables
	function get_users()
    {        

        $edit = '<button name="edit" type="button" class="btn-outline gray btn-sm" data-toggle="modal" data-target="#modal_update" data-id="$1">Edit</button>';
        $delete = '<button name="delete" type="button" class="btn-outline danger btn-sm" data-toggle="modal" data-target="#modal_delete" data-id="$1">Delete</button>';

        $this->load->library('datatables');
        $this->datatables->select("users.id as id,CONCAT(users.first_name,'&nbsp;',users.last_name) as name, users.email as email, user_types.name as type,
        	CASE
        		WHEN user_types.type_id = 2 THEN 'NA'
        		WHEN user_types.type_id = 3 THEN users.has_exam
        	END as exam_status, users.batch as batch, CONCAT(users2.fname,'&nbsp;',users2.lname) as created_by")
        	// users.has_exam as exam_status, users.batch as batch, CONCAT(users2.fname,'&nbsp;',users2.lname) as created_by")
                 ->from('users')
                 ->join('user_types', 'user_types.type_id=users.user_type','left')
                 ->join('(SELECT users.id as uid, users.first_name as fname, users.last_name as lname FROM users) as users2', 'users2.uid=users.created_by','left')
                 ->where('users.user_type != 1');
                if($this->SuperAdmin)
    			{
    				$this->datatables->add_column('actions', $edit . $delete, 'id');
    			}   

        // $this->datatables->select("users.id as id,CONCAT(users.first_name,'&nbsp;',users.last_name) as name, users.email as email, user_types.name as type")
        //                  ->from('users')
        //                  ->join('user_types', 'user_types.type_id=users.user_type')
        //                  ->where('users.user_type != 1');                         
            			// if($this->SuperAdmin)
            			// {
            			// 	$this->datatables->add_column('actions', $edit . $delete, 'id');
            			// }
            			// else if ($this->Admin)
            			// {
            			// 	$this->datatables->where('users.user_type != 2')->add_column('actions', $edit . $delete, 'id');
            			// }
            			// else
            			// {
            			// 	// Pass some errors
            			// }

        echo $this->datatables->generate();
    }

	/**
	 * Redirect if needed, otherwise display the user list
	 */
	public function index()
	{

		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		else if ($this->Student) // remove this elseif if you want to enable this for non-admins
		{
			// redirect them to the home page because they must be an administrator to view this
			show_error('You must be an administrator to view this page.');
		}
		else
		{
			$tables = $this->config->item('tables', 'ion_auth');
			$identity_column = $this->config->item('identity', 'ion_auth');
			$this->data['identity_column'] = $identity_column;

			// display the create user form
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

			$this->data['first_name'] = [
				'name' => 'first_name',
				'id' => 'first_name',
				'type' => 'text',
				'placeholder' => 'First Name',
				'value' => $this->form_validation->set_value('first_name'),
			];
			$this->data['middle_name'] = [
				'name' => 'middle_name',
				'id' => 'middle_name',
				'type' => 'text',
				'placeholder' => 'Middle Name',
				'value' => $this->form_validation->set_value('middle_name'),
			];
			$this->data['last_name'] = [
				'name' => 'last_name',
				'id' => 'last_name',
				'type' => 'text',
				'placeholder' => 'Last Name',
				'value' => $this->form_validation->set_value('last_name'),
			];
			$this->data['identity'] = [
				'name' => 'identity',
				'id' => 'identity',
				'type' => 'text',
				'value' => $this->form_validation->set_value('identity'),
			];
			$this->data['email'] = [
				'name' => 'email',
				'id' => 'email',
				'type' => 'text',
				'placeholder' => 'Username',
				'value' => $this->form_validation->set_value('email'),
			];
			$this->data['company'] = [
				'name' => 'company',
				'id' => 'company',
				'type' => 'text',
				'placeholder' => 'Company',
				'value' => $this->form_validation->set_value('company'),
			];
			$this->data['phone'] = [
				'name' => 'phone',
				'id' => 'phone',
				'type' => 'text',
				'placeholder' => 'Contact No.',
				'value' => $this->form_validation->set_value('phone'),
			];
			$this->data['batch'] = [
				'name' => 'batch',
				'id' => 'batch',
				'type' => 'text',
				'placeholder' => 'Batch No.',
				'value' => $this->form_validation->set_value('batch'),
			];
			$this->data['password'] = [
				'name' => 'password',
				'id' => 'password',
				'type' => 'password',
				'placeholder' => 'Password',
				'value' => $this->form_validation->set_value('password'),
			];
			$this->data['password_confirm'] = [
				'name' => 'password_confirm',
				'id' => 'password_confirm',
				'type' => 'password',
				'placeholder' => 'Confirm Password',
				'value' => $this->form_validation->set_value('password_confirm'),
			];

			$this->data['user_types'] = $this->ion_auth->get_user_types();

			$this->data['title'] = $this->lang->line('index_heading');
			
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			//list the users
			$this->data['users'] = $this->ion_auth->users()->result();

			$this->data['last_insert_id'] = $this->ion_auth->get_last_insert_id()->id + 1;
			
			//USAGE NOTE - you can do more complicated queries like this
			//$this->data['users'] = $this->ion_auth->where('field', 'value')->users()->result();
			
			foreach ($this->data['users'] as $k => $user)
			{
				$this->data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
			}

			$this->load->view('header',$this->data);
			$this->load->view('auth/index', $this->data);
			$this->load->view('footer',$this->data);
		}
	}

	/**
	 * Log the user in
	 */
	public function login()
	{
		if (!$this->ion_auth->logged_in())
		{
			$this->data['title'] = $this->lang->line('login_heading');

			// validate form input
			$this->form_validation->set_rules('identity', str_replace(':', '', $this->lang->line('login_identity_label')), 'required');
			$this->form_validation->set_rules('password', str_replace(':', '', $this->lang->line('login_password_label')), 'required');

			if ($this->form_validation->run() === TRUE)
			{
				// check to see if the user is logging in
				// check for "remember me"
				$remember = (bool)$this->input->post('remember');

				if ($this->ion_auth->login($this->input->post('identity'), $this->input->post('password'), $remember))
				{
					//if the login is successful
					//redirect them back to the home page
					$this->session->set_flashdata('message', $this->ion_auth->messages());

					$history_data = array(
						'user_id' => $this->session->userdata('user_id'),
						'date' => date('Y-m-d H:i:s'),
						'activity' => 'Logged In',
					);

					$result = $this->db->insert('log_history', $history_data);
					
					if($this->ion_auth->in_type('Admin') || $this->ion_auth->in_type('SuperAdmin'))
					{				
						redirect('/welcome','refresh');
					}
					else
					{
						redirect('/student','refresh');
					}
				}
				else
				{
					// if the login was un-successful
					// redirect them back to the login page
					$this->session->set_flashdata('message', $this->ion_auth->errors());
					redirect('auth/login', 'refresh'); // use redirects instead of loading views for compatibility with MY_Controller libraries
				}
			}
			else
			{
				// the user is not logging in so display the login page
				// set the flash data error message if there is one
				$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

				$this->data['identity'] = [
					'name' => 'identity',
					'id' => 'identity',
					'type' => 'text',
					'placeholder' => 'Username',
					'required' => 'required',
					'value' => $this->form_validation->set_value('identity'),
				];

				$this->data['password'] = [
					'name' => 'password',
					'id' => 'password',
					'type' => 'password',
					'required' => 'required',
					'placeholder'=>'Password',
				];

				$this->_render_page('auth' . DIRECTORY_SEPARATOR . 'login', $this->data);
			}
		}
		else
		{
			if($this->ion_auth->in_type('Student'))
			{
				redirect('student','refresh');
			}
			else
			{				
				redirect('welcome','refresh');				
			}
		}
	}

	/**
	 * Log the user out
	 */
	public function logout()
	{
		$this->data['title'] = "Logout";

		$history_data = array(
			'user_id' => $this->session->userdata('user_id'),
			'date' => date('Y-m-d H:i:s'),
			'activity' => 'Logged Out',
		);

		$result = $this->db->insert('log_history', $history_data);
		// log the user out
		$this->ion_auth->logout();

		// redirect them to the login page
		$this->session->set_flashdata('message', $this->ion_auth->messages());
		redirect('auth/login', 'refresh');
	}

	/**
	 * Change password
	 */
	public function change_password()
	{
		$this->form_validation->set_rules('old', $this->lang->line('change_password_validation_old_password_label'), 'required');
		$this->form_validation->set_rules('new', $this->lang->line('change_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|matches[new_confirm]');
		$this->form_validation->set_rules('new_confirm', $this->lang->line('change_password_validation_new_password_confirm_label'), 'required');

		if (!$this->ion_auth->logged_in())
		{
			redirect('auth/login', 'refresh');
		}

		$user = $this->ion_auth->user()->row();

		if ($this->form_validation->run() === FALSE)
		{
			// display the form
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			$this->data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
			$this->data['old_password'] = [
				'name' => 'old',
				'id' => 'old',
				'type' => 'password',
			];
			$this->data['new_password'] = [
				'name' => 'new',
				'id' => 'new',
				'type' => 'password',
				'pattern' => '^.{' . $this->data['min_password_length'] . '}.*$',
			];
			$this->data['new_password_confirm'] = [
				'name' => 'new_confirm',
				'id' => 'new_confirm',
				'type' => 'password',
				'pattern' => '^.{' . $this->data['min_password_length'] . '}.*$',
			];
			$this->data['user_id'] = [
				'name' => 'user_id',
				'id' => 'user_id',
				'type' => 'hidden',
				'value' => $user->id,
			];

			// render
			$this->_render_page('auth' . DIRECTORY_SEPARATOR . 'change_password', $this->data);
		}
		else
		{
			$identity = $this->session->userdata('identity');

			$change = $this->ion_auth->change_password($identity, $this->input->post('old'), $this->input->post('new'));

			if ($change)
			{
				//if the password was successfully changed
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				$this->logout();
			}
			else
			{
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect('auth/change_password', 'refresh');
			}
		}
	}

	/**
	 * Forgot password
	 */
	public function forgot_password()
	{
		$this->data['title'] = $this->lang->line('forgot_password_heading');
		
		// setting validation rules by checking whether identity is username or email
		if ($this->config->item('identity', 'ion_auth') != 'email')
		{
			$this->form_validation->set_rules('identity', $this->lang->line('forgot_password_identity_label'), 'required');
		}
		else
		{
			$this->form_validation->set_rules('identity', $this->lang->line('forgot_password_validation_email_label'), 'required|valid_email');
		}


		if ($this->form_validation->run() === FALSE)
		{
			$this->data['type'] = $this->config->item('identity', 'ion_auth');
			// setup the input
			$this->data['identity'] = [
				'name' => 'identity',
				'id' => 'identity',
			];

			if ($this->config->item('identity', 'ion_auth') != 'email')
			{
				$this->data['identity_label'] = $this->lang->line('forgot_password_identity_label');
			}
			else
			{
				$this->data['identity_label'] = $this->lang->line('forgot_password_email_identity_label');
			}

			// set any errors and display the form
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');
			$this->_render_page('auth' . DIRECTORY_SEPARATOR . 'forgot_password', $this->data);
		}
		else
		{
			$identity_column = $this->config->item('identity', 'ion_auth');
			$identity = $this->ion_auth->where($identity_column, $this->input->post('identity'))->users()->row();

			if (empty($identity))
			{

				if ($this->config->item('identity', 'ion_auth') != 'email')
				{
					$this->ion_auth->set_error('forgot_password_identity_not_found');
				}
				else
				{
					$this->ion_auth->set_error('forgot_password_email_not_found');
				}

				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect("auth/forgot_password", 'refresh');
			}

			// run the forgotten password method to email an activation code to the user
			$forgotten = $this->ion_auth->forgotten_password($identity->{$this->config->item('identity', 'ion_auth')});

			if ($forgotten)
			{
				// if there were no errors
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect("auth/login", 'refresh'); //we should display a confirmation page here instead of the login page
			}
			else
			{
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect("auth/forgot_password", 'refresh');
			}
		}
	}

	/**
	 * Reset password - final step for forgotten password
	 *
	 * @param string|null $code The reset code
	 */
	public function reset_password($code = NULL)
	{
		if (!$code)
		{
			show_404();
		}

		$this->data['title'] = $this->lang->line('reset_password_heading');
		
		$user = $this->ion_auth->forgotten_password_check($code);

		if ($user)
		{
			// if the code is valid then display the password reset form

			$this->form_validation->set_rules('new', $this->lang->line('reset_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|matches[new_confirm]');
			$this->form_validation->set_rules('new_confirm', $this->lang->line('reset_password_validation_new_password_confirm_label'), 'required');

			if ($this->form_validation->run() === FALSE)
			{
				// display the form

				// set the flash data error message if there is one
				$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

				$this->data['min_password_length'] = $this->config->item('min_password_length', 'ion_auth');
				$this->data['new_password'] = [
					'name' => 'new',
					'id' => 'new',
					'type' => 'password',
					'pattern' => '^.{' . $this->data['min_password_length'] . '}.*$',
				];
				$this->data['new_password_confirm'] = [
					'name' => 'new_confirm',
					'id' => 'new_confirm',
					'type' => 'password',
					'pattern' => '^.{' . $this->data['min_password_length'] . '}.*$',
				];
				$this->data['user_id'] = [
					'name' => 'user_id',
					'id' => 'user_id',
					'type' => 'hidden',
					'value' => $user->id,
				];
				$this->data['csrf'] = $this->_get_csrf_nonce();
				$this->data['code'] = $code;

				// render
				$this->_render_page('auth' . DIRECTORY_SEPARATOR . 'reset_password', $this->data);
			}
			else
			{
				$identity = $user->{$this->config->item('identity', 'ion_auth')};

				// do we have a valid request?
				if ($this->_valid_csrf_nonce() === FALSE || $user->id != $this->input->post('user_id'))
				{

					// something fishy might be up
					$this->ion_auth->clear_forgotten_password_code($identity);

					show_error($this->lang->line('error_csrf'));

				}
				else
				{
					// finally change the password
					$change = $this->ion_auth->reset_password($identity, $this->input->post('new'));

					if ($change)
					{
						// if the password was successfully changed
						$this->session->set_flashdata('message', $this->ion_auth->messages());
						redirect("auth/login", 'refresh');
					}
					else
					{
						$this->session->set_flashdata('message', $this->ion_auth->errors());
						redirect('auth/reset_password/' . $code, 'refresh');
					}
				}
			}
		}
		else
		{
			// if the code is invalid then send them back to the forgot password page
			$this->session->set_flashdata('message', $this->ion_auth->errors());
			redirect("auth/forgot_password", 'refresh');
		}
	}

	/**
	 * Activate the user
	 *
	 * @param int         $id   The user ID
	 * @param string|bool $code The activation code
	 */
	public function activate($id, $code = FALSE)
	{
		$activation = FALSE;

		if ($code !== FALSE)
		{
			$activation = $this->ion_auth->activate($id, $code);
		}
		else if ($this->ion_auth->is_admin())
		{
			$activation = $this->ion_auth->activate($id);
		}

		if ($activation)
		{
			// redirect them to the auth page
			$this->session->set_flashdata('message', $this->ion_auth->messages());
			redirect("auth", 'refresh');
		}
		else
		{
			// redirect them to the forgot password page
			$this->session->set_flashdata('message', $this->ion_auth->errors());
			redirect("auth/forgot_password", 'refresh');
		}
	}

	/**
	 * Deactivate the user
	 *
	 * @param int|string|null $id The user ID
	 */
	public function deactivate($id = NULL)
	{
		if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
		{
			// redirect them to the home page because they must be an administrator to view this
			show_error('You must be an administrator to view this page.');
		}

		$id = (int)$id;

		$this->load->library('form_validation');
		$this->form_validation->set_rules('confirm', $this->lang->line('deactivate_validation_confirm_label'), 'required');
		$this->form_validation->set_rules('id', $this->lang->line('deactivate_validation_user_id_label'), 'required|alpha_numeric');

		if ($this->form_validation->run() === FALSE)
		{
			// insert csrf check
			$this->data['csrf'] = $this->_get_csrf_nonce();
			$this->data['user'] = $this->ion_auth->user($id)->row();

			$this->_render_page('auth' . DIRECTORY_SEPARATOR . 'deactivate_user', $this->data);
		}
		else
		{
			// do we really want to deactivate?
			if ($this->input->post('confirm') == 'yes')
			{
				// do we have a valid request?
				if ($this->_valid_csrf_nonce() === FALSE || $id != $this->input->post('id'))
				{
					show_error($this->lang->line('error_csrf'));
				}

				// do we have the right userlevel?
				if ($this->ion_auth->logged_in() && $this->ion_auth->is_admin())
				{
					$this->ion_auth->deactivate($id);
				}
			}

			// redirect them back to the auth page
			redirect('auth', 'refresh');
		}
	}

	/**
	 * Create a new user
	 */
	public function create_user()
	{
		$this->data['title'] = $this->lang->line('create_user_heading');

		if (!$this->ion_auth->logged_in())
		{
			redirect('auth', 'refresh');
		}

		$tables = $this->config->item('tables', 'ion_auth');
		$identity_column = $this->config->item('identity', 'ion_auth');
		$this->data['identity_column'] = $identity_column;

		// validate form input
		$this->form_validation->set_rules('batch', 'Batch', 'trim|numeric');
		$this->form_validation->set_rules('first_name', $this->lang->line('create_user_validation_fname_label'), 'trim|required|regex_match[/^[a-z .\-]+$/i]');
		$this->form_validation->set_rules('middle_name', $this->lang->line('create_user_validation_mname_label'), 'trim|required|regex_match[/^[a-z .\-]+$/i]');
		$this->form_validation->set_rules('last_name', $this->lang->line('create_user_validation_lname_label'), 'trim|required|regex_match[/^[a-z .\-]+$/i]');
		if ($identity_column !== 'email')
		{
			$this->form_validation->set_rules('identity', $this->lang->line('create_user_validation_identity_label'), 'trim|required|is_unique[' . $tables['users'] . '.' . $identity_column . ']');
			$this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'trim|required|is_unique');
		}
		else
		{
			$this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'trim|required|is_unique[' . $tables['users'] . '.email]');
		}
		$this->form_validation->set_rules('phone', $this->lang->line('create_user_validation_contact_label'),'trim|max_length[9]|numeric');
		$this->form_validation->set_rules('password', $this->lang->line('create_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|matches[password_confirm]');
		$this->form_validation->set_rules('password_confirm', $this->lang->line('create_user_validation_password_confirm_label'), 'required');
		$this->form_validation->set_rules('user_type', 'User Type', 'required');

		if ($this->form_validation->run() === TRUE)
		{
			$email = strtolower($this->input->post('email'));
			$identity = ($identity_column === 'email') ? $email : $this->input->post('identity');
			$password = $this->input->post('password');

			$additional_data = [
				'first_name' => $this->input->post('first_name'),
				'last_name' => $this->input->post('last_name'),
				'middle_name' => $this->input->post('middle_name'),
				'user_type' => $this->input->post('user_type'),
				'phone' => $this->input->post('phone'),
				'batch' => $this->input->post('batch'),
				'created_by' => $this->session->userdata('user_id'),
			];
		}
		else
		{
			$this->data['message'] = $this->session->set_flashdata('error', validation_errors());
			redirect("auth", $this->data);	
		}
		if ($this->form_validation->run() === TRUE && $this->ion_auth->register($identity, $password, $email, $additional_data))
		{
			// check to see if we are creating the user
			// redirect them back to the admin page
			$history_data = array(
				'user_id' => $this->session->userdata('user_id'),
				'date' => date('Y-m-d H:i:s'),
				'activity' => 'Create User - ' . $additional_data['first_name'] . ' ' . $additional_data['last_name'],
			);

			$result = $this->db->insert('log_history', $history_data);
			$this->data['message'] = $this->session->set_flashdata('success', $this->ion_auth->messages());
			redirect("auth", $this->data);
		}
		else
		{
			//Redirect to auth and pass error
			
			$tables = $this->config->item('tables', 'ion_auth');
			$identity_column = $this->config->item('identity', 'ion_auth');
			$this->data['identity_column'] = $identity_column;

			// display the create user form
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

			$this->data['first_name'] = [
				'name' => 'first_name',
				'id' => 'first_name',
				'type' => 'text',
				'placeholder' => 'First Name',
				'value' => $this->form_validation->set_value('first_name'),
			];
			$this->data['middle_name'] = [
				'name' => 'middle_name',
				'id' => 'middle_name',
				'type' => 'text',
				'placeholder' => 'Middle Name',
				'value' => $this->form_validation->set_value('middle_name'),
			];
			$this->data['last_name'] = [
				'name' => 'last_name',
				'id' => 'last_name',
				'type' => 'text',
				'placeholder' => 'Last Name',
				'value' => $this->form_validation->set_value('last_name'),
			];
			$this->data['identity'] = [
				'name' => 'identity',
				'id' => 'identity',
				'type' => 'text',
				'value' => $this->form_validation->set_value('identity'),
			];
			$this->data['email'] = [
				'name' => 'email',
				'id' => 'email',
				'type' => 'text',
				'placeholder' => 'Username',
				'value' => $this->form_validation->set_value('email'),
			];
			$this->data['phone'] = [
				'name' => 'phone',
				'id' => 'phone',
				'type' => 'text',
				'placeholder' => 'Contact No.',
				'value' => $this->form_validation->set_value('phone'),
			];
			$this->data['batch'] = [
				'name' => 'batch',
				'id' => 'batch',
				'type' => 'text',
				'placeholder' => 'Batch No.',
				'value' => $this->form_validation->set_value('batch'),
			];			
			$this->data['password'] = [
				'name' => 'password',
				'id' => 'password',
				'type' => 'password',
				'placeholder' => 'Password',
				'value' => $this->form_validation->set_value('password'),
			];
			$this->data['password_confirm'] = [
				'name' => 'password_confirm',
				'id' => 'password_confirm',
				'type' => 'password',
				'placeholder' => 'Confirm Password',
				'value' => $this->form_validation->set_value('password_confirm'),
			];

			$this->data['user_types'] = $this->ion_auth->get_user_types();

			$this->data['title'] = $this->lang->line('index_heading');
			
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			//list the users
			$this->data['users'] = $this->ion_auth->users()->result();
			
			//USAGE NOTE - you can do more complicated queries like this
			//$this->data['users'] = $this->ion_auth->where('field', 'value')->users()->result();
			
			foreach ($this->data['users'] as $k => $user)
			{
				$this->data['users'][$k]->groups = $this->ion_auth->get_users_groups($user->id)->result();
			}

			$this->load->view('header',$this->data);
			$this->load->view('auth/index', $this->data);
			$this->load->view('footer',$this->data);
		}
	}
	/**
	* Redirect a user checking if is admin
	*/
	public function redirectUser(){
		if ($this->ion_auth->is_admin()){
			redirect('auth', 'refresh');
		}
		redirect('/', 'refresh');
	}

	/**
	 * Edit a user
	 *
	 * @param int|string $id
	 */
	public function edit_user()
	{
		$this->data['title'] = $this->lang->line('edit_user_heading');

		// if (!$this->ion_auth->logged_in() || (!$this->ion_auth->is_admin() && !($this->ion_auth->user()->row()->id == $id)))
		if (!$this->ion_auth->logged_in() && !($this->ion_auth->user()->row()->id == $id))
		{
			redirect('auth', 'refresh');
		}

		$id = $this->input->post('id');
		$user = $this->ion_auth->user($id)->row();
		$groups = $this->ion_auth->groups()->result_array();
		$currentGroups = $this->ion_auth->get_users_groups($id)->result();
			
		//USAGE NOTE - you can do more complicated queries like this
		//$groups = $this->ion_auth->where(['field' => 'value'])->groups()->result_array();
	

		// validate form input
		$this->form_validation->set_rules('first_name', $this->lang->line('edit_user_validation_fname_label'), 'trim|required|regex_match[/^[a-z .\-]+$/i]');
		$this->form_validation->set_rules('middle_name', $this->lang->line('edit_user_validation_mname_label'), 'trim|required|regex_match[/^[a-z .\-]+$/i]');
		$this->form_validation->set_rules('last_name', $this->lang->line('edit_user_validation_lname_label'), 'trim|required|regex_match[/^[a-z .\-]+$/i]');
		$this->form_validation->set_rules('phone', $this->lang->line('edit_user_validation_phone_label'),'trim|max_length[9]|numeric');
		$this->form_validation->set_rules('batch', $this->lang->line('edit_user_validation_batch_label'),'trim|numeric');
		$this->form_validation->set_rules('company', $this->lang->line('edit_user_validation_company_label'), 'trim');

		if (isset($_POST) && !empty($_POST))
		{
			// do we have a valid request?
			if ($this->_valid_csrf_nonce() === FALSE || $id != $this->input->post('id'))
			{
				show_error($this->lang->line('error_csrf'));
			}

			// update the password if it was posted
			if ($this->input->post('password'))
			{
				$this->form_validation->set_rules('password', $this->lang->line('edit_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|matches[password_confirm]');
				$this->form_validation->set_rules('password_confirm', $this->lang->line('edit_user_validation_password_confirm_label'), 'required');
			}

			if ($this->form_validation->run() === TRUE)
			{
				$data = [
					'first_name' => $this->input->post('first_name'),
					'middle_name' => $this->input->post('middle_name'),
					'last_name' => $this->input->post('last_name'),
					'phone' => $this->input->post('phone'),
				];

				// update the password if it was posted
				if ($this->input->post('password'))
				{
					$data['password'] = $this->input->post('password');
				}

				// Only allow updating groups if user is admin
				if ($this->ion_auth->is_admin())
				{
					// Update the groups user belongs to
					$this->ion_auth->remove_from_group('', $id);
					
					$groupData = $this->input->post('groups');
					if (isset($groupData) && !empty($groupData))
					{
						foreach ($groupData as $grp)
						{
							$this->ion_auth->add_to_group($grp, $id);
						}

					}
				}

				// check to see if we are updating the user
				if ($this->ion_auth->update($user->id, $data))
				{
					// redirect them back to the admin page if admin, or to the base url if non admin
					$history_data = array(
						'user_id' => $this->session->userdata('user_id'),
						'date' => date('Y-m-d H:i:s'),
						'activity' => 'Update User',
					);

					$result = $this->db->insert('log_history', $history_data);
					$this->data['message'] = $this->session->set_flashdata('success', $this->ion_auth->messages());
					redirect('auth',$this->data);

				}
				else
				{
					// redirect them back to the admin page if admin, or to the base url if non admin
					$this->data['message'] = $this->session->set_flashdata('error', $this->ion_auth->errors());
					redirect('auth',$this->data);

				}

			}
		}

		// display the edit user form
		$this->data['csrf'] = $this->_get_csrf_nonce();

		// set the flash data error message if there is one
		$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

		// pass the user to the view
		$this->data['user'] = $user;
		$this->data['groups'] = $groups;
		$this->data['currentGroups'] = $currentGroups;

		$this->data['first_name'] = [
			'name'  => 'first_name',
			'id'    => 'first_name',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('first_name', $user->first_name),
		];
		$this->data['middle_name'] = [
			'name'  => 'middle_name',
			'id'    => 'middle_name',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('middle_name', $user->middle_name),
		];
		$this->data['last_name'] = [
			'name'  => 'last_name',
			'id'    => 'last_name',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('last_name', $user->last_name),
		];
		$this->data['company'] = [
			'name'  => 'company',
			'id'    => 'company',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('company', $user->company),
		];
		$this->data['phone'] = [
			'name'  => 'phone',
			'id'    => 'phone',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('phone', $user->phone),
		];
		$this->data['batch'] = [
			'name'  => 'batch',
			'id'    => 'batch',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('batch', $user->batch),
		];
		$this->data['password'] = [
			'name' => 'password',
			'id'   => 'password',
			'type' => 'password'
		];
		$this->data['password_confirm'] = [
			'name' => 'password_confirm',
			'id'   => 'password_confirm',
			'type' => 'password'
		];

		$this->_render_page('auth/edit_user', $this->data);
	}

	/**
	* Edit/Update User via modal
	* @author Thesis
	*/
	public function update()
    {
        if ( ! $this->input->is_ajax_request())
        {
            exit('No direct script access allowed.');
        }
        else
        {
			$id = $this->input->post('id');
			$user = $this->ion_auth->user($id)->row();
			$groups = $this->ion_auth->groups()->result_array();
			$currentGroups = $this->ion_auth->get_users_groups($id)->result();
	        // validate form input
			$this->form_validation->set_rules('first_name', $this->lang->line('edit_user_validation_fname_label'), 'trim|required|regex_match[/^[a-z .\-]+$/i]');
			$this->form_validation->set_rules('middle_name', $this->lang->line('edit_user_validation_mname_label'), 'trim|required|regex_match[/^[a-z .\-]+$/i]');
			$this->form_validation->set_rules('last_name', $this->lang->line('edit_user_validation_lname_label'), 'trim|required|regex_match[/^[a-z .\-]+$/i]');
			$this->form_validation->set_rules('phone', $this->lang->line('edit_user_validation_phone_label'),'trim|max_length[11]|numeric');
			$this->form_validation->set_rules('batch', $this->lang->line('edit_user_validation_batch_label'),'trim|numeric');
			$this->form_validation->set_rules('company', $this->lang->line('edit_user_validation_company_label'), 'trim');
			$this->form_validation->set_rules('user_type', 'User Type', 'required');

			if (isset($_POST) && !empty($_POST))
			{
				// do we have a valid request?
				if ($this->_valid_csrf_nonce() === FALSE)
				{
					show_error($this->lang->line('error_csrf'));
				}

				// update the password if it was posted
				if ($this->input->post('password'))
				{
					$this->form_validation->set_rules('password', $this->lang->line('edit_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|matches[password_confirm]');
					$this->form_validation->set_rules('password_confirm', $this->lang->line('edit_user_validation_password_confirm_label'), 'required');
				}

				if ($this->form_validation->run() === TRUE)
				{
					$data = [
						'first_name' => $this->input->post('first_name'),
						'last_name' => $this->input->post('last_name'),
						'company' => $this->input->post('company'),
						'phone' => $this->input->post('phone'),
						'user_type' => $this->input->post('user_type'),
						'batch' => $this->input->post('batch'),
					];

					// update the password if it was posted
					if ($this->input->post('password'))
					{
						$data['password'] = $this->input->post('password');
					}

					// Only allow updating groups if user is admin
					if ($this->ion_auth->is_admin())
					{
						// Update the groups user belongs to
						$this->ion_auth->remove_from_group('', $id);
						
						$groupData = $this->input->post('groups');
						if (isset($groupData) && !empty($groupData))
						{
							foreach ($groupData as $grp)
							{
								$this->ion_auth->add_to_group($grp, $id);
							}

						}
					}

					// check to see if we are updating the user
					if ($this->ion_auth->update($user->id, $data))
					{
						$history_data = array(
							'user_id' => $this->session->userdata('user_id'),
							'date' => date('Y-m-d H:i:s'),
							'activity' => 'Update User - ' . $data['first_name'] . ' ' . $data['last_name'],
						);

						$result = $this->db->insert('log_history', $history_data);
						$output = [
                        	'result'  => 'success',
                        	'message' => $this->ion_auth->messages()
	                    ];

					}
					else
					{
						$output = [
                        	'result'  => 'error',
                        	'message' => $this->ion_auth->errors()
                    	];

					}

				}
			}
            else
            {
                $output = [
                    'result'  => 'error',
                    'message' => validation_errors()
                ];
            }

            echo json_encode($output);
            exit();
        }
    }
	public function edit()
	{
		if ( ! $this->input->is_ajax_request())
        {
            exit('No direct script access allowed.');
        }
        else
        {
            $id = $this->input->post('id');
            $user = $this->ion_auth->user($id)->row();
			$groups = $this->ion_auth->groups()->result_array();
			$currentGroups = $this->ion_auth->get_users_groups($id)->result();
			
			// display the edit user form
			$this->data['csrf'] = $this->_get_csrf_nonce();

			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

			// pass the user to the view
			$this->data['user'] = $user;
			$this->data['groups'] = $groups;
			$this->data['currentGroups'] = $currentGroups;
			$this->data['user_types'] = $this->ion_auth->get_user_types();

			$this->data['first_name'] = [
				'name'  => 'first_name',
				'id'    => 'first_name',
				'type'  => 'text',
				'placeholder' => 'First Name',
				'value' => $this->form_validation->set_value('first_name', $user->first_name),
			];
			$this->data['middle_name'] = [
				'name'  => 'middle_name',
				'id'    => 'middle_name',
				'type'  => 'text',
				'placeholder' => 'Middle Name',
				'value' => $this->form_validation->set_value('middle_name', $user->middle_name),
			];
			$this->data['last_name'] = [
				'name'  => 'last_name',
				'id'    => 'last_name',
				'type'  => 'text',
				'placeholder' => 'Last Name',
				'value' => $this->form_validation->set_value('last_name', $user->last_name),
			];
			$this->data['company'] = [
				'name'  => 'company',
				'id'    => 'company',
				'type'  => 'text',
				'placeholder' => 'Company',
				'value' => $this->form_validation->set_value('company', $user->company),
			];
			$this->data['phone'] = [
				'name'  => 'phone',
				'id'    => 'phone',
				'type'  => 'text',
				'placeholder' => 'Contact No.',
				'value' => $this->form_validation->set_value('phone', $user->phone),
			];
			$this->data['batch'] = [
				'name'  => 'batch',
				'id'    => 'batch',
				'type'  => 'text',
				'placeholder' => 'Batch No.',
				'value' => $this->form_validation->set_value('batch', $user->batch),
			];			
			$this->data['password'] = [
				'name' => 'password',
				'id'   => 'password',
				'type' => 'password',
				'placeholder' => 'Password',
			];
			$this->data['password_confirm'] = [
				'name' => 'password_confirm',
				'id'   => 'password_confirm',
				'type' => 'password',
				'placeholder' => 'Confirm Password',
			];

            $output = [
                'result' => ($this->data['user']) ? 'success' : 'error',
                'html'   => $this->load->view('auth/edit', $this->data, TRUE)
            ];

            echo json_encode($output);
            exit();
        }
	}

	/**
	* Delete User
	* @author Thesis
	*/
	public function delete()
    {
        if ( ! $this->input->is_ajax_request())
        {
            exit('No direct script access allowed.');
        }
        else
        {
            $id = $this->input->post('id');
            $user = $this->site->get_user($id);
            if ($this->ion_auth_model->delete_user($id))
            {
				$history_data = array(
					'user_id' => $this->session->userdata('user_id'),
					'date' => date('Y-m-d H:i:s'),
					'activity' => 'Delete User - ' . $user->first_name . ' ' . $user->last_name,
				);

				$result = $this->db->insert('log_history', $history_data);
                $output = [
                    'result'  => 'success',
                    'message' => $this->ion_auth->messages()
                ];

                $this->site->delete_student_result($id);
            }
            else
            {
                $output = [
                    'result'  => 'error',
                    'message' => $this->ion_auth->errors()
                ];
            }
        }

        echo json_encode($output);
        exit();
    }

	/**
	 * Create a new group
	 */
	public function create_group()
	{
		$this->data['title'] = $this->lang->line('create_group_title');

		if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
		{
			redirect('auth', 'refresh');
		}

		// validate form input
		$this->form_validation->set_rules('group_name', $this->lang->line('create_group_validation_name_label'), 'trim|required|alpha_dash');

		if ($this->form_validation->run() === TRUE)
		{
			$new_group_id = $this->ion_auth->create_group($this->input->post('group_name'), $this->input->post('description'));
			if ($new_group_id)
			{
				// check to see if we are creating the group
				// redirect them back to the admin page
				$this->session->set_flashdata('message', $this->ion_auth->messages());
				redirect("auth", 'refresh');
			}
		}
		else
		{
			// display the create group form
			// set the flash data error message if there is one
			$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

			$this->data['group_name'] = [
				'name'  => 'group_name',
				'id'    => 'group_name',
				'type'  => 'text',
				'value' => $this->form_validation->set_value('group_name'),
			];
			$this->data['description'] = [
				'name'  => 'description',
				'id'    => 'description',
				'type'  => 'text',
				'value' => $this->form_validation->set_value('description'),
			];

			$this->_render_page('auth/create_group', $this->data);
		}
	}

	/**
	 * Edit a group
	 *
	 * @param int|string $id
	 */
	public function edit_group($id)
	{
		// bail if no group id given
		if (!$id || empty($id))
		{
			redirect('auth', 'refresh');
		}

		$this->data['title'] = $this->lang->line('edit_group_title');

		if (!$this->ion_auth->logged_in() || !$this->ion_auth->is_admin())
		{
			redirect('auth', 'refresh');
		}

		$group = $this->ion_auth->group($id)->row();

		// validate form input
		$this->form_validation->set_rules('group_name', $this->lang->line('edit_group_validation_name_label'), 'trim|required|alpha_dash');

		if (isset($_POST) && !empty($_POST))
		{
			if ($this->form_validation->run() === TRUE)
			{
				$group_update = $this->ion_auth->update_group($id, $_POST['group_name'], array(
					'description' => $_POST['group_description']
				));

				if ($group_update)
				{
					$this->session->set_flashdata('message', $this->lang->line('edit_group_saved'));
				}
				else
				{
					$this->session->set_flashdata('message', $this->ion_auth->errors());
				}
				redirect("auth", 'refresh');
			}
		}

		// set the flash data error message if there is one
		$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

		// pass the user to the view
		$this->data['group'] = $group;

		$this->data['group_name'] = [
			'name'    => 'group_name',
			'id'      => 'group_name',
			'type'    => 'text',
			'value'   => $this->form_validation->set_value('group_name', $group->name),
		];
		if ($this->config->item('admin_group', 'ion_auth') === $group->name) {
			$this->data['group_name']['readonly'] = 'readonly';
		}
		
		$this->data['group_description'] = [
			'name'  => 'group_description',
			'id'    => 'group_description',
			'type'  => 'text',
			'value' => $this->form_validation->set_value('group_description', $group->description),
		];

		$this->_render_page('auth' . DIRECTORY_SEPARATOR . 'edit_group', $this->data);
	}

	/**
	 * @return array A CSRF key-value pair
	 */
	public function _get_csrf_nonce()
	{
		$this->load->helper('string');
		$key = random_string('alnum', 8);
		$value = random_string('alnum', 20);
		$this->session->set_flashdata('csrfkey', $key);
		$this->session->set_flashdata('csrfvalue', $value);

		return [$key => $value];
	}

	/**
	 * @return bool Whether the posted CSRF token matches
	 */
	public function _valid_csrf_nonce(){
		$csrfkey = $this->input->post($this->session->flashdata('csrfkey'));
		if ($csrfkey && $csrfkey === $this->session->flashdata('csrfvalue'))
		{
			return TRUE;
		}
			return FALSE;
	}

	/**
	 * @param string     $view
	 * @param array|null $data
	 * @param bool       $returnhtml
	 *
	 * @return mixed
	 */
	public function _render_page($view, $data = NULL, $returnhtml = FALSE)//I think this makes more sense
	{

		$viewdata = (empty($data)) ? $this->data : $data;

		$view_html = $this->load->view($view, $viewdata, $returnhtml);

		// This will return html on 3rd argument being true
		if ($returnhtml)
		{
			return $view_html;
		}
	}

		/**
	 * Register Student then login
	 */
	public function register_student()
	{
		$this->data['title'] = $this->lang->line('create_user_heading');

		$tables = $this->config->item('tables', 'ion_auth');
		$identity_column = $this->config->item('identity', 'ion_auth');
		$this->data['identity_column'] = $identity_column;

		// validate form input
		$this->form_validation->set_rules('first_name', $this->lang->line('create_user_validation_fname_label'), 'trim|required');
		$this->form_validation->set_rules('middle_name', $this->lang->line('create_user_validation_mname_label'), 'trim|required');
		$this->form_validation->set_rules('last_name', $this->lang->line('create_user_validation_lname_label'), 'trim|required');
		if ($identity_column !== 'email')
		{
			$this->form_validation->set_rules('identity', $this->lang->line('create_user_validation_identity_label'), 'trim|required|is_unique[' . $tables['users'] . '.' . $identity_column . ']');
			$this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'trim|required|valid_email');
		}
		else
		{
			$this->form_validation->set_rules('email', $this->lang->line('create_user_validation_email_label'), 'trim|required|valid_email|is_unique[' . $tables['users'] . '.email]');
		}
		$this->form_validation->set_rules('password', $this->lang->line('create_user_validation_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|matches[password_confirm]');
		$this->form_validation->set_rules('password_confirm', $this->lang->line('create_user_validation_password_confirm_label'), 'required');

		if ($this->form_validation->run() === TRUE)
		{
			$email = strtolower($this->input->post('email'));
			$identity = ($identity_column === 'email') ? $email : $this->input->post('identity');
			$password = $this->input->post('password');

			$additional_data = [
				'first_name' => $this->input->post('first_name'),
				'middle_name' => $this->input->post('middle_name'),
				'last_name' => $this->input->post('last_name'),
				'user_type' => 2, // Automatically store as student
			];
			if ($this->ion_auth->register($identity, $password, $email, $additional_data))
			{
				// check to see if we are creating the user
				//login student
				if($this->ion_auth->login($identity, $password))
				{
					redirect('/student','refresh');
				}
			}
			else
			{
				//pass error in login page
				$this->session->set_flashdata('message', $this->ion_auth->errors());
				redirect('auth/login', 'refresh');
			}
		}
		else
		{
			//pass error when form validation is false
			$this->data['message'] = (validation_errors()) ? validation_errors() : $this->session->flashdata('message');

			$this->data['identity'] = [
				'name' => 'identity',
				'id' => 'identity',
				'type' => 'text',
				'placeholder' => 'Email',
				'required' => 'required',
				'value' => $this->form_validation->set_value('identity'),
			];

			$this->data['password'] = [
				'name' => 'password',
				'id' => 'password',
				'type' => 'password',
				'required' => 'required',
				'placeholder'=>'Password',
			];
			
			$this->load->view('auth/login', $this->data);
		}
	}

	public function import_user()
	{
		$this->load->model('ion_auth_model');

		$data = file_get_contents($_FILES['importfile']['tmp_name']);
		//Convert JSON string into PHP array format
		$data_array = json_decode($data, true); 
		foreach($data_array as $row) {
		    //Build multiple insert query
			$last_insert_id = $this->ion_auth->get_last_insert_id()->id + 1;
			$id = str_pad($last_insert_id,5,'0',STR_PAD_LEFT);
			$year = date('Y');

			$student_id = $year . '_' . $id;

		    $import_user = array(
				'username' 		=> $student_id,
				'email'	   		=> $student_id,
				'active'   		=> 1,
				'user_type'		=> 3,
				'password'		=> $this->ion_auth_model->hash_password($student_id),
				'first_name' 	=> $row['first_name'],
				'middle_name'	=> $row['middle_name'],
				'last_name'		=> $row['last_name'],
				'phone'			=> $row['phone']
		    );
		          

		    if($this->db->insert('users',$import_user))
		    {
		    	$this->data['message'] = $this->session->set_flashdata('success','Import Successfully!');

		    	
		    }
		    else
		    {
		    	$this->data['message'] = $this->session->set_flashdata('error','Import Failed! Please check your JSON file for duplicate entry of username.');
		    }
		    
		    
		}

		redirect('auth',$this->data);

	}

	function import_csv()
    {
        $this->load->helper('security');
        $this->form_validation->set_rules('userfile', 'Upload File', 'xss_clean');

        if ($this->form_validation->run() == true) {

            if (isset($_FILES["userfile"])) {

                $this->load->library('upload');
                $config['upload_path'] = $this->digital_upload_path;
                $config['allowed_types'] = 'csv';
                $config['max_size'] = $this->allowed_file_size;
                $config['overwrite'] = TRUE;
                $config['encrypt_name'] = TRUE;
                $config['max_filename'] = 25;
                $this->upload->initialize($config);

                if (!$this->upload->do_upload()) {
                    $error = $this->upload->display_errors();
                    $this->session->set_flashdata('error', $error);
                    redirect("auth");
                }

                $csv = $this->upload->file_name;

                $arrResult = array();
                $handle = fopen($this->digital_upload_path . $csv, "r");
                if ($handle) {
                    while (($row = fgetcsv($handle, 5000, ",")) !== FALSE) {
                        $arrResult[] = $row;
                    }
                    fclose($handle);
                }
                $titles = array_shift($arrResult);
                $updated = 0; $items = array();
                foreach ($arrResult as $key => $value) {

    			$last_insert_id = $this->ion_auth->get_last_insert_id()->id + 1;
				$id = str_pad($last_insert_id,5,'0',STR_PAD_LEFT);
				$year = date('Y');

				$student_id = $year . '_' . $id;

                    $item = [
                        'username'              => $student_id,
                        'email'           		=> $student_id,
                        'active'  				=> 1,
                        'user_type'             => 3,
                        'password' 				=> $this->ion_auth_model->hash_password($student_id),
                        'first_name'            => isset($value[0]) ? trim($value[0]) : '',
                        'middle_name'     		=> isset($value[1]) ? trim($value[1]) : '',
                        'last_name'             => isset($value[2]) ? trim($value[2]) : '',
                        'phone'         		=> isset($value[3]) ? trim($value[3]) : '',
                        'batch'         		=> isset($value[4]) ? trim($value[4]) : '',
                        'created_by'    		=> $this->session->userdata('user_id'),
                    ];

	                if($this->db->insert('users',$item))
				    {
				    	$this->data['message'] = $this->session->set_flashdata('success','Import Successfully!');

				    	
				    }
				    else
				    {
				    	$this->data['message'] = $this->session->set_flashdata('error','Import Failed! Please check your JSON file for duplicate entry of username.');
				    }
                }
		    	$history_data = array(
					'user_id' => $this->session->userdata('user_id'),
					'date' => date('Y-m-d H:i:s'),
					'activity' => 'Import Users',
				);

				$result = $this->db->insert('log_history', $history_data);
                redirect('auth',$this->data);
            }
        }
    }

    function check_email_avalibility()  
    {
        if($this->ion_auth_model->is_email_available($_POST["email"]))  
        {  
            echo '<label class="text-danger"><span class="glyphicon glyphicon-remove"></span> Username Already exists!</label>';  
        }  
        else  
        {  
            echo '<label class="text-success"><span class="glyphicon glyphicon-ok"></span>Username available</label>';  
        }  
    }

}
