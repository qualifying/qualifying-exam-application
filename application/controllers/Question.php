<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Question extends MY_Controller
{
    function __construct()
    {
            parent::__construct();
            $this->load->model('question_model');
    }
    
    function index($category_id = NULL)
    {
        $this->data['categories'] = $this->question_model->get_categories();
        $this->data['category_id'] = $category_id;
        $this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));

        $this->load->view('header',$this->data);
        $this->load->view('question/index',$this->data);
        $this->load->view('footer',$this->data);
    }

    function get_questions($category_id = NULL)
    {
        

        $buttons = '
                    <button name="edit" type="button" class="btn-outline gray btn-sm" data-toggle="modal" data-target="#modal_update" data-id="$1">Edit</button>
                    <button name="delete" type="button" class="btn-outline danger btn-sm" data-toggle="modal" data-target="#modal_delete" data-id="$1">Delete</button>
                    ';

        $this->load->library('datatables');

        $this->datatables->select("questions.id as question_id, question, options.value as answer, categories.name as category")
                         ->from('questions')
                         ->join('options', 'questions.answer = options.option_id','left')
                         ->join('categories', 'questions.category = categories.category_id', 'left');
        if($category_id)
        {
            $this->datatables->where('categories.category_id',$category_id);
        }


        $this->datatables->add_column('actions', $buttons, 'question_id');

        echo $this->datatables->generate();
    }
    
    function add_question()
    {
        $this->form_validation->set_rules('question', $this->lang->line('question_validation'), 'required|is_unique[questions.question]');
        $this->form_validation->set_rules('radioChoice', 'Choice', 'required');
        $this->form_validation->set_rules('categories', 'Category', 'required');

        if ($this->form_validation->run() === TRUE)
        { 
            $choice = $this->input->post('radioChoice');
            $category = $this->input->post('categories');   
            $question_data = array(
                    'question'      => $this->input->post('question'),
                    'category'      => $category,
                    'answer'        => 0,
                    );
            if($this->question_model->add_question($question_data) == true)
            {

                $question_id = $this->db->insert_id();

                $options = $this->input->post('radioOptions');
                foreach ($options as $option)
                {
                    $option_data = array(
                        'question_id'   => $question_id,
                        'value'         => $option,
                        );

                    if($this->question_model->add_option($option_data) == true)
                    {
                        if($choice == $option)
                        {
                            $answer = $this->db->insert_id();
                        }
                    }
                }     

            }

            $data = array(
                            'answer' => $answer
                            );

            $this->db->where('id', $question_id);
            // $this->db->update('questions', $data);
            if($this->db->update('questions', $data))
            $history_data = array(
                    'user_id' => $this->session->userdata('user_id'),
                    'date' => date('Y-m-d H:i:s'),
                    'activity' => 'Add Question ID # ' . $question_id,
                );

            $result = $this->db->insert('log_history', $history_data);
            $this->data['message'] = $this->session->set_flashdata('success', 'Question added successfully!');
            redirect("question", $this->data);  
        }
        else
        {
            $this->data['message'] = $this->session->set_flashdata('error', validation_errors());
            redirect("question", $this->data); 
        }
    }

    // Edit Department Form
    public function edit_question()
    {
        if ( ! $this->input->is_ajax_request())
        {
            exit('No direct script access allowed.');
        }
        else
        {
            $id = $this->input->post('id');
            $this->data['categories'] = $this->question_model->get_categories();
            $this->data['row'] = $this->question_model->get_question($id);

            if ($this->data['row'])
            {
                $this->data['question'] = [
                    'name'          => 'e_question',
                    'id'            => 'e_question',
                    'rows'          => '5',
                    'placeholder'   => 'Question'
                ];

                $this->data['option'] = [
                    'name'          => 'e_option',
                    'id'            => 'e_option',
                    'type'          => 'text',
                    'placeholder'   => 'Option/s'
                ];
            }

            $this->data['choices'] = $this->question_model->get_options($id);

            if ($this->data['choices'])
            {
                $this->data['r_choice'] = [
                    'class'     => 'icheck',
                    'name'      => 'radioChoice',
                    'required'  => 'required'
                ];

                $this->data['h_choice'] = [
                    'class'     => 'icheck',
                    'name'      => 'radioChoice',
                    'required'  => 'required'
                ];
            }

            $output = [
                'result' => ($this->data['row']) ? 'success' : 'error',
                'html'   => $this->load->view('question/edit', $this->data, TRUE)
            ];

            echo json_encode($output);
            exit();
        }
    }

    // Update Department
    public function update()
    {
        if ( ! $this->input->is_ajax_request())
        {
            exit('No direct script access allowed.');
        }
        else
        {
            $this->form_validation->set_rules('e_question', $this->lang->line('question_validation'), 'required|is_unique[questions.question]');
            $this->form_validation->set_rules('radioChoice', 'Choice', 'required');
            $this->form_validation->set_rules('categories', 'Category', 'required');

            if ($this->form_validation->run() === TRUE)
            {      
                $question_id = $this->input->post('question_id');
                $choice = $this->input->post('radioChoice');
                $category = $this->input->post('categories');

                $options = $this->question_model->get_options($question_id);
                $answer = NULL;
                foreach ($options as $option)
                {
                    $option_id = $option->option_id;
                    $e_opt = $this->input->post('e_opt'.$option_id);

                    $edit_option_data = [
                        'question_id'   => $question_id,
                        'value'         => $e_opt,
                    ];

                    if($this->question_model->update_option($option_id,$edit_option_data) === TRUE)
                    {
                        if($option_id == $choice)
                        {
                            $answer = $option_id;
                        }                
                    }
                }

                $additional_options = $this->input->post('radioOptions');

                if($additional_options)
                {
                    foreach ($additional_options as $additional)
                    {
                        $additional_option_data = [
                            'question_id'   => $question_id,
                            'value'         => $additional,
                        ];

                        if($this->question_model->add_option($additional_option_data) === TRUE)
                        {
                            if($choice == $additional)
                            {
                                $answer = $this->db->insert_id();
                            }
                        }
                    }                
                }


                $question_data = [
                    'question' => $this->input->post('e_question'),
                    'category' => $category,
                    'answer'   => $answer,
                ];

                if($this->question_model->update_question($question_id, $question_data) === TRUE)
                {
                    $history_data = array(
                        'user_id' => $this->session->userdata('user_id'),
                        'date' => date('Y-m-d H:i:s'),
                        'activity' => 'Update Question ID # ' . $question_id,
                    );

                    $result = $this->db->insert('log_history', $history_data);

                    $output = [
                        'result'  => 'success',
                        'message' => $this->ion_auth->messages()
                    ];
                }
                else
                {
                    $output = [
                        'result'  => 'error',
                        'message' => $this->ion_auth->errors()
                    ];
                }
            }

            else
            {
                $output = [
                    'result'  => 'error',
                    'message' => validation_errors()
                ];
            }

            echo json_encode($output);
            exit();
        }
    }

    public function delete()
    {
        if ( ! $this->input->is_ajax_request())
        {
            exit('No direct script access allowed.');
        }
        else
        {
            $id = $this->input->post('id');

            if ($this->question_model->delete_question($id))
            {
                $history_data = array(
                        'user_id' => $this->session->userdata('user_id'),
                        'date' => date('Y-m-d H:i:s'),
                        'activity' => 'Delete Question ID # ' . $id,
                );

                $result = $this->db->insert('log_history', $history_data);

                $output = [
                    'result'  => 'success',
                    'message' => $this->ion_auth->messages()
                ];
            }
            else
            {
                $output = [
                    'result'  => 'error',
                    'message' => $this->ion_auth->errors()
                ];
            }
        }

        echo json_encode($output);
        exit();
    }
    function check_question_avalibility()  
    {
        if($this->question_model->is_question_available($_POST["question"]))  
        {  
            echo '<label class="text-danger"><span class="glyphicon glyphicon-remove"></span> Question Already exists!</label>';  
        }  
        else  
        {  
            echo '<label class="text-success"><span class="glyphicon glyphicon-ok"></span>Question available</label>';  
        }  
    }

    function check_e_question_avalibility()  
    {
        if($this->question_model->is_question_available($_POST["equestion"]))  
        {  
            echo '<label class="text-danger"><span class="glyphicon glyphicon-remove"></span> Question Already exists!</label>';  
        }  
        else  
        {  
            echo '<label class="text-success"><span class="glyphicon glyphicon-ok"></span>Question available</label>';  
        }  
    }
}

    
