<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Student extends MY_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	function __construct()
    {
        parent::__construct();
        $this->load->model('student_model');
    }
	function index()
	{
		if (!$this->ion_auth->logged_in())
		{
			// redirect them to the login page
			redirect('auth/login', 'refresh');
		}
		else
		{
			$this->load->view('header',$this->data);
			$this->load->view('student/instruction_dashboard');
			$this->load->view('footer',$this->data);			
		}
	}

	function exam()
	{
		if(!$this->ion_auth->logged_in())
		{
			redirect('auth/login', 'refresh');
		}
		else
		{
			$student_id = $this->session->userdata('user_id');
			$exam_date = date('Y-m-d');

			if ($this->student_model->has_exam($this->session->userdata('user_id')))
			{
				redirect('student');
			}

			$insert_data = array(
				'has_exam' => 1,
				'start_date' => $exam_date,
			);

			$this->student_model->insert_has_exam($student_id, $insert_data);

			$student_exam_end = $this->student_model->get_student_exam_end($this->session->userdata('user_id'));

			if(is_null($student_exam_end))
			{
				$exam_duration = $this->site->get_exam_duration()->exam_duration;

				$current_date = date('Y-m-d H:i:s');
				
				$current_date_ts = strtotime($current_date);
				$end_date_ts = $current_date_ts + ($exam_duration * 60);

				$end_date = date('Y-m-d H:i:s',$end_date_ts);

				$datetime = array(
					'end_date' => $end_date
				);

				if($this->student_model->student_exam_end($this->session->userdata('user_id'),$datetime))
				{					
					$insert_data = array(
						'has_exam' => 1,
					);

					$this->student_model->insert_has_exam($student_id, $insert_data);
				}

					$history_data = array(
						'user_id' => $this->session->userdata('user_id'),
						'date' => date('Y-m-d H:i:s'),
						'activity' => 'Examination',
					);

					$result = $this->db->insert('log_history', $history_data);
				
			}

			$student_data = $this->student_model->get_student_data($this->session->userdata('user_id'));

			$this->data['remaining_time'] = $end_date_ts - $current_date_ts;
			
			$this->data['student_id'] = $student_data->id;

			$this->data['questions'] = $this->student_model->get_questions();

			$this->load->view('header',$this->data);
			$this->load->view('student/questionnaire',$this->data);
			$this->load->view('footer',$this->data);	
		}
	}

	public function answers()
	{
		$student_id = $this->session->userdata('user_id');

		$questions = $this->student_model->get_questions();
		$categories = $this->student_model->get_categories();

		$correct_answer = 0;

		foreach($categories as $data)
		{
			$correct_answer_in[$data->category_id] = 0;
		}

		foreach($questions as $row)
		{
			$cat_id = $row->category;

			$choice = $this->input->post('choice_in_question_id_' . $row->id);
			
			if($choice == $row->answer)
			{
				$correct_answer++;
				$correct_answer_in[$cat_id]++;
			}
		}

		foreach($categories as $category)
		{
			$number_of_question_in = $this->student_model->get_number_of_question_in($category->category_id);
			$passing = $number_of_question_in * 0.5;

			if($correct_answer_in[$category->category_id] > $passing)
			{
				$course_passed[$category->category_id] = $category->name;
				$course_passed_ = array(
					'category_id' => $category->category_id,
					'student_id' => $student_id,
				);

				// insert to db course_passed
				$this->student_model->insert_course_passed($course_passed_);
			}
		}

		if($course_passed)
		{
			$courses = implode(",", $course_passed);
		}
		else
		{
			$courses = "NA";
		}

		// $number_of_categories = $this->student_model->get_number_of_categories();
		$number_of_questions = $this->student_model->get_number_of_questions();

		$average = $correct_answer / $number_of_questions * 100;

		if($average > 80)
		{
			$passed = 1;
		}
		else
		{
			$passed = 0;
		}

		$student_results = array(
			'student_id' => $student_id,
			'result'	 => $correct_answer,
			'average'	 => $average,
			'course_passed' => $courses,
			'passed'	 => $passed,
		);

		if($this->student_model->insert($student_results))
		{
			redirect('student','refresh');
		}
	}

    // get student_results
    public function get_student_results()
    {
        $this->load->library('datatables');

        $this->datatables->select('id, CONCAT(first_name,"&nbsp;",last_name) as name, start_date, phone, CONCAT(student_results.result," out of ",(SELECT COUNT(id) from questions)) as result_total, CONCAT(student_results.average, "%") as average, student_results.course_passed as course_passed, 
        	student_results.passed as result, users.batch as batch')
                         ->from('users')
                         ->join('student_results','users.id=student_results.student_id','left')
                         ->where('users.has_exam',1);

        echo $this->datatables->generate();
    }

    // show student_results
    public function student_results()
    {
        $this->load->view('header',$this->data);
        $this->load->view('student/results');
        $this->load->view('footer',$this->data);
    }

    public function bulk_actions()
    {
    	$this->form_validation->set_rules('form_action', lang("form_action"), 'required');

    	if ($this->form_validation->run() == true)
    	{
    		if (!empty($_POST['val']))
    		{
    			if ($this->input->post('form_action') == 'export_excel_')
    			{
    				$this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle('Student Results');
                    $this->excel->getActiveSheet()->SetCellValue('B1', '                        Republic of the Philippines');
                    $this->excel->getActiveSheet()->SetCellValue('B2', '                                         DHVSU');
                    $this->excel->getActiveSheet()->SetCellValue('B3', '                             Bacolor, Pampanga');
                    $this->excel->getActiveSheet()->SetCellValue('B4', '                           College of Education');
                    $this->excel->getActiveSheet()->SetCellValue('B5', '                  Qualifying Examination Result');

                    $this->excel->getActiveSheet()->SetCellValue('A7', '#');
                    $this->excel->getActiveSheet()->SetCellValue('B7', 'Student Name');
                    $this->excel->getActiveSheet()->SetCellValue('C7', 'Examination Date');
                    $this->excel->getActiveSheet()->SetCellValue('D7', 'Total');
                    $this->excel->getActiveSheet()->SetCellValue('E7', 'Average');
                    $this->excel->getActiveSheet()->SetCellValue('F7', 'Course Passed');
                    $this->excel->getActiveSheet()->SetCellValue('G7', 'Result');
                    $this->excel->getActiveSheet()->SetCellValue('H7', 'Batch No.');


                    $row = 8;
                    $student_no = 1;
    				foreach ($_POST['val'] as $id)
    				{
    					$student = $this->student_model->get_student_data($id);
    					$student_result = $this->student_model->get_student_results($id);
    					$number_of_questions = $this->student_model->get_number_of_questions();

    					$this->excel->getActiveSheet()->SetCellValue('A' . $row, $student_no);
    					$this->excel->getActiveSheet()->SetCellValue('B' . $row, $student->first_name . ' ' . $student->last_name);
    					$this->excel->getActiveSheet()->SetCellValue('C' . $row, $student->start_date);
                        $this->excel->getActiveSheet()->SetCellValue('D' . $row, $student_result->result . ' / ' . $number_of_questions);
                        $this->excel->getActiveSheet()->SetCellValue('E' . $row, $student_result->average);
                        $this->excel->getActiveSheet()->SetCellValue('F' . $row, $student_result->course_passed);
                        $this->excel->getActiveSheet()->SetCellValue('G' . $row, ($student_result->passed == 1 ? 'Passed' : 'Failed'));
                        $this->excel->getActiveSheet()->SetCellValue('H' . $row, $student->batch);
    					$student_no++;
                        $row++;
    				}
    				$date = date('Y-m-d');
    				$user = $this->site->get_user($this->session->userdata('user_id'));

    				$this->excel->getActiveSheet()->SetCellValue('G' . $row, '______________');
    				$row++;

    				$this->excel->getActiveSheet()->SetCellValue('G' . $row, 'Name');
    				$this->excel->getActiveSheet()->SetCellValue('H' . $row, 'Date');
                    $row++;
    				
    				$this->excel->getActiveSheet()->SetCellValue('G' . $row, $user->first_name .' '.$user->last_name);
    				$this->excel->getActiveSheet()->SetCellValue('H' . $row, $date);

    				$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
    				$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(50);
    				$this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
    				$this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(10);
    				$this->excel->getActiveSheet()->getColumnDimension('E')->setWidth(10);
    				$this->excel->getActiveSheet()->getColumnDimension('F')->setWidth(25);
    				$this->excel->getActiveSheet()->getColumnDimension('G')->setWidth(25);
    				$this->excel->getActiveSheet()->getColumnDimension('H')->setWidth(15);
                    $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $filename = 'student_result_' . date('Y_m_d_H_i_s');
                    $this->load->helper('excel');
                    $history_data = array(
						'user_id' => $this->session->userdata('user_id'),
						'date' => date('Y-m-d H:i:s'),
						'activity' => 'Export Excel (Selected)',
					);

					$result = $this->db->insert('log_history', $history_data);
                    create_excel($this->excel, $filename);
    			}
    		}
    		else
    		{
    			if ($this->input->post('form_action') == 'export_excel')
    			{
    				$this->load->library('excel');
                    $this->excel->setActiveSheetIndex(0);
                    $this->excel->getActiveSheet()->setTitle('Student Results');
                    $this->excel->getActiveSheet()->SetCellValue('B1', '                        Republic of the Philippines');
                    $this->excel->getActiveSheet()->SetCellValue('B2', '                                         DHVSU');
                    $this->excel->getActiveSheet()->SetCellValue('B3', '                             Bacolor, Pampanga');
                    $this->excel->getActiveSheet()->SetCellValue('B4', '                           College of Education');
                    $this->excel->getActiveSheet()->SetCellValue('B5', '                  Qualifying Examination Result');
                    $this->excel->getActiveSheet()->SetCellValue('A7', '#');
                    $this->excel->getActiveSheet()->SetCellValue('B7', 'Student Name');
                    $this->excel->getActiveSheet()->SetCellValue('C7', 'Batch No.');               

                    $row = 8;
                    $student_no = 1;

                    $student_passed = $this->student_model->get_students_passed();

    				foreach ($student_passed as $student)
    				{
    					$this->excel->getActiveSheet()->SetCellValue('A' . $row, $student_no);
    					$this->excel->getActiveSheet()->SetCellValue('B' . $row, $student->first_name . ' ' . $student->middle_name . ' ' . $student->last_name);
    					$this->excel->getActiveSheet()->SetCellValue('C' . $row, $student->batch);
    					$student_no++;
                        $row++;
    				}

    				$date = date('Y-m-d');
    				$user = $this->site->get_user($this->session->userdata('user_id'));

    				$this->excel->getActiveSheet()->SetCellValue('C' . $row, '______________');
    				$row++;

    				$this->excel->getActiveSheet()->SetCellValue('C' . $row, 'Name');
    				$this->excel->getActiveSheet()->SetCellValue('D' . $row, 'Date');
                    $row++;
    				
    				$this->excel->getActiveSheet()->SetCellValue('C' . $row, $user->first_name .' '.$user->last_name);
    				$this->excel->getActiveSheet()->SetCellValue('D' . $row, $date);

    				$this->excel->getActiveSheet()->getColumnDimension('A')->setWidth(5);
    				$this->excel->getActiveSheet()->getColumnDimension('B')->setWidth(50);
    				$this->excel->getActiveSheet()->getColumnDimension('C')->setWidth(15);
    				$this->excel->getActiveSheet()->getColumnDimension('D')->setWidth(15);
                    $this->excel->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
                    $filename = 'student_result_' . date('Y_m_d_H_i_s');
                    $this->load->helper('excel');
                    $history_data = array(
						'user_id' => $this->session->userdata('user_id'),
						'date' => date('Y-m-d H:i:s'),
						'activity' => 'Export Excel (Passed)',
					);

					$result = $this->db->insert('log_history', $history_data);
                    create_excel($this->excel, $filename);

    			}
    			else if ($this->input->post('form_action') == 'generate_pdf')
    			{
			        $this->data['student_passed'] = $this->student_model->get_students_passed();
			        $this->data['date'] = date('M d,Y');
			        $this->data['user'] = $this->site->get_user($this->session->userdata('user_id'));

			        $name = "Results_" . date('Y_m_d_H_i_s') .".pdf";
			        $html = $this->load->view('student/results_pdf',$this->data, true);
			           
			        $this->sma->generate_pdf($html, $name, false, NULL, NULL, NULL, NULL, 'P', 'Letter');
			        $history_data = array(
						'user_id' => $this->session->userdata('user_id'),
						'date' => date('Y-m-d H:i:s'),
						'activity' => 'Generate PDF',
					);

					$result = $this->db->insert('log_history', $history_data);
    			}
    			
    			else
    			{
	                $this->data['message'] = $this->session->set_flashdata('error', 'Please select student first');
	                redirect('student/student_results','refresh');    				
    			}
            }
    	}
    	else
    	{
            $this->session->set_flashdata('result','error');
            $this->session->set_flashdata('message', validation_errors());
            redirect('student/student_results','refresh');
        }
    }
}
