<?php defined('BASEPATH') OR exit('No direct script access allowed');

class History extends MY_Controller
{
    function __construct()
    {
            parent::__construct();
    }
    
    function index()
    {
            $this->load->view('header',$this->data);
            $this->load->view('history/index');
            $this->load->view('footer',$this->data);
    }

    function get_history()
    {
        $this->load->library('datatables');

        $this->datatables->select('CONCAT(users.first_name,"&nbsp;",users.middle_name,"&nbsp;",users.last_name) as user_fullname,log_history.date as activity_date,activity')
                         ->from('log_history')
                         ->join('users','log_history.user_id=users.id','left');

        echo $this->datatables->generate();
    }
}

    
