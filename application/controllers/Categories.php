<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Categories extends MY_Controller
{
    function __construct()
    {
            parent::__construct();
            $this->load->model('category_model');
    }
    
    function index()
    {
            $this->load->view('header',$this->data);
            $this->load->view('category/index');
            $this->load->view('footer',$this->data);
    }

    function get_categories()
    {
        

        $buttons = '
                    <button name="edit" type="button" class="btn-outline gray btn-sm" data-toggle="modal" data-target="#modal_update" data-id="$1">Edit</button>
                    <button name="delete" type="button" class="btn-outline danger btn-sm" data-toggle="modal" data-target="#modal_delete" data-id="$1">Delete</button>
                    ';

        $this->load->library('datatables');

        $this->datatables->select("categories.category_id as id, name, description")
                         ->from('categories')
                         ->add_column('actions', $buttons, 'id');

        echo $this->datatables->generate();
    }
    
    function add_category()
    {
        $category_data = array(
                'name'              => $this->input->post('name'),
                'description'       => $this->input->post('description'),
                );
        if($this->category_model->add_category($category_data) == true)
        {
             $history_data = array(
                'user_id' => $this->session->userdata('user_id'),
                'date' => date('Y-m-d H:i:s'),
                'activity' => 'Add Category ' . $category_data['name'],
            );   
            $result = $this->db->insert('log_history', $history_data);
        }

      redirect('categories');
    }

    // Edit Department Form
    public function edit_category()
    {
        if ( ! $this->input->is_ajax_request())
        {
            exit('No direct script access allowed.');
        }
        else
        {
            $id = $this->input->post('id');

            $this->data['row'] = $this->category_model->get_category($id);

            if ($this->data['row'])
            {
                $this->data['name'] = [
                    'name' => 'name',
                    'id'   => 'name',
                    'type' => 'text'
                ];

                $this->data['description'] = [
                    'name' => 'description',
                    'id'   => 'description',
                    'rows' => 5
                ];
            }

            $output = [
                'result' => ($this->data['row']) ? 'success' : 'error',
                'html'   => $this->load->view('category/edit', $this->data, TRUE)
            ];

            echo json_encode($output);
            exit();
        }
    }

    // Update Department
    public function update()
    {
        if ( ! $this->input->is_ajax_request())
        {
            exit('No direct script access allowed.');
        }
        else
        {
            $this->form_validation->set_rules('name', 'Name', 'required');
            $category_id = $this->input->post('category_id');
            if ($this->form_validation->run() === TRUE)
            {
                $category_data = [
                    'name'          => $this->input->post('name'),
                    'description'   => $this->input->post('description'),
                ];

                if($this->category_model->update_category($category_id, $category_data) === TRUE)
                {
                     $history_data = array(
                        'user_id' => $this->session->userdata('user_id'),
                        'date' => date('Y-m-d H:i:s'),
                        'activity' => 'Update Category '. $category_data['name'],
                    );
                    $result = $this->db->insert('log_history', $history_data);   
                    $output = [
                        'result'  => 'success',
                        'message' => $this->ion_auth->messages()
                    ];
                }
                else
                {
                    $output = [
                        'result'  => 'error',
                        'message' => $this->ion_auth->errors()
                    ];
                }
            }

            else
            {
                $output = [
                    'result'  => 'error',
                    'message' => validation_errors()
                ];
            }

            echo json_encode($output);
            exit();
        }
    }

    public function delete()
    {
        if ( ! $this->input->is_ajax_request())
        {
            exit('No direct script access allowed.');
        }
        else
        {
            $id = $this->input->post('id');
            $category_name = $this->category_model->get_category($id)->name;
            if ($this->category_model->delete_category($id))
            {
                 $history_data = array(
                    'user_id' => $this->session->userdata('user_id'),
                    'date' => date('Y-m-d H:i:s'),
                    'activity' => 'Delete Category ' . $category_name,
                );
                $result = $this->db->insert('log_history', $history_data);   
                $output = [
                    'result'  => 'success',
                    'message' => $this->ion_auth->messages()
                ];
            }
            else
            {
                $output = [
                    'result'  => 'error',
                    'message' => $this->ion_auth->errors()
                ];
            }
        }

        echo json_encode($output);
        exit();
    }
}

    
