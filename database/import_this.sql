ALTER TABLE `qualifying_exam_app`.`users` ADD COLUMN `created_by` INT(11) NOT NULL AFTER `has_exam`;

ALTER TABLE `qualifying_exam_app`.`users` ADD COLUMN `batch` INT(11) NOT NULL AFTER `created_by`;