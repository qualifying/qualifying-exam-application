-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 18, 2019 at 10:08 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `qualifying_exam_app`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `category_id` int(11) UNSIGNED NOT NULL,
  `name` varchar(55) NOT NULL,
  `description` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`category_id`, `name`, `description`) VALUES
(2, 'BSEd-English', 'Major in English'),
(3, 'BSEd-Math', 'Major in Math'),
(4, 'BPE', 'Major in Physical Education'),
(6, 'BSEd-Filipino', 'Major in Filipino');

-- --------------------------------------------------------

--
-- Table structure for table `course_passed`
--

CREATE TABLE `course_passed` (
  `category_id` int(11) UNSIGNED NOT NULL,
  `student_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` mediumint(8) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'members', 'General User');

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `login_attempts`
--

INSERT INTO `login_attempts` (`id`, `ip_address`, `login`, `time`) VALUES
(41, '::1', 'admin', 1576660038),
(42, '::1', 'admin@admin.com', 1576660046);

-- --------------------------------------------------------

--
-- Table structure for table `log_history`
--

CREATE TABLE `log_history` (
  `history_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `date` timestamp NULL DEFAULT NULL,
  `activity` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `log_history`
--

INSERT INTO `log_history` (`history_id`, `user_id`, `date`, `activity`) VALUES
(146, 1, '2019-10-22 12:49:07', 'Logged In'),
(147, 1, '2019-10-22 12:50:03', 'Create User - Carl Louis Calma'),
(148, 1, '2019-10-22 13:08:13', 'Logged Out'),
(149, 1, '2019-10-22 13:08:32', 'Logged In'),
(150, 1, '2019-10-22 13:27:57', 'Logged Out'),
(151, 1, '2019-10-22 13:28:15', 'Logged In'),
(152, 1, '2019-10-22 13:29:35', 'Create User - Juana Cruz'),
(153, 1, '2019-10-22 14:07:31', 'Import Users'),
(154, 1, '2019-10-22 14:09:00', 'Import Users'),
(155, 1, '2019-10-22 14:11:06', 'Import Users'),
(156, 1, '2019-10-22 14:12:43', 'Import Users'),
(157, 1, '2019-10-22 14:13:48', 'Import Users'),
(158, 1, '2019-10-22 15:03:51', 'Export Excel (Selected)'),
(159, 1, '2019-10-22 15:10:21', 'Export Excel (Selected)'),
(160, 1, '2019-10-22 15:16:42', 'Generate PDF'),
(161, 1, '2019-10-22 15:44:44', 'Export Excel (Passed)'),
(162, 1, '2019-10-22 15:45:54', 'Export Excel (Passed)'),
(163, 1, '2019-10-22 15:50:29', 'Generate PDF'),
(164, 1, '2019-10-22 15:50:58', 'Generate PDF'),
(165, 1, '2019-10-22 15:51:35', 'Generate PDF'),
(166, 1, '2019-10-22 15:51:58', 'Export Excel (Passed)'),
(167, 1, '2019-10-22 15:52:15', 'Export Excel (Selected)'),
(168, 1, '2019-10-23 00:38:23', 'Logged In'),
(169, 1, '2019-10-23 00:39:16', 'Create User - Carl Ctus'),
(170, 348, '2019-10-23 00:39:26', 'Logged In'),
(171, 1, '2019-10-23 01:01:50', 'Create User - Admin Admin'),
(172, 1, '2019-10-24 00:23:45', 'Logged In'),
(173, 1, '2019-10-24 00:26:00', 'Logged Out'),
(174, 1, '2019-10-24 00:26:10', 'Logged In'),
(175, 1, '2019-10-24 03:25:26', 'Logged In'),
(176, 1, '2019-12-18 09:08:08', 'Logged In');

-- --------------------------------------------------------

--
-- Table structure for table `options`
--

CREATE TABLE `options` (
  `option_id` int(11) UNSIGNED NOT NULL,
  `question_id` int(11) UNSIGNED NOT NULL,
  `value` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE `questions` (
  `id` int(11) UNSIGNED NOT NULL,
  `question` text NOT NULL,
  `answer` int(11) UNSIGNED NOT NULL,
  `category` smallint(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `exam_duration` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`exam_duration`) VALUES
(60);

-- --------------------------------------------------------

--
-- Table structure for table `student_results`
--

CREATE TABLE `student_results` (
  `student_id` int(11) UNSIGNED NOT NULL,
  `result` decimal(12,0) DEFAULT NULL,
  `average` decimal(12,0) DEFAULT NULL,
  `course_passed` varchar(255) DEFAULT NULL,
  `passed` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student_results`
--

INSERT INTO `student_results` (`student_id`, `result`, `average`, `course_passed`, `passed`) VALUES
(95, '50', '100', 'ce', 1),
(96, '50', '100', 'ce', 1),
(297, '50', '100', 'ce', 1),
(298, '50', '100', 'ce', 1),
(299, '50', '100', 'ce', 1),
(300, '50', '100', 'ce', 1),
(301, '50', '100', 'ce', 1),
(302, '50', '100', 'ce', 1),
(303, '50', '100', 'ce', 1),
(304, '50', '100', 'ce', 1),
(305, '50', '100', 'ce', 1),
(306, '50', '100', 'ce', 1),
(307, '50', '100', 'ce', 1),
(308, '50', '100', 'ce', 1),
(309, '50', '100', 'ce', 1),
(310, '50', '100', 'ce', 1),
(311, '50', '100', 'ce', 1),
(312, '50', '100', 'ce', 1),
(313, '50', '100', 'ce', 1),
(314, '50', '100', 'ce', 1),
(315, '50', '100', 'ce', 1),
(316, '50', '100', 'ce', 1),
(317, '50', '100', 'ce', 1),
(318, '50', '100', 'ce', 1),
(319, '50', '100', 'ce', 1),
(320, '50', '100', 'ce', 1),
(321, '50', '100', 'ce', 1),
(322, '50', '100', 'ce', 1),
(323, '50', '100', 'ce', 1),
(324, '50', '100', 'ce', 1),
(325, '50', '100', 'ce', 1),
(326, '50', '100', 'ce', 1),
(327, '50', '100', 'ce', 1),
(328, '50', '100', 'ce', 1),
(329, '50', '100', 'ce', 1),
(330, '50', '100', 'ce', 1),
(331, '50', '100', 'ce', 1),
(332, '50', '100', 'ce', 1),
(333, '50', '100', 'ce', 1),
(334, '50', '100', 'ce', 1),
(335, '50', '100', 'ce', 1),
(336, '50', '100', 'ce', 1),
(337, '50', '100', 'ce', 1),
(338, '50', '100', 'ce', 1),
(339, '50', '100', 'ce', 1),
(340, '50', '100', 'ce', 1),
(341, '50', '100', 'ce', 1),
(342, '50', '100', 'ce', 1),
(343, '50', '100', 'ce', 1),
(344, '50', '100', 'ce', 1),
(345, '50', '100', 'ce', 1),
(346, '50', '100', 'ce', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(254) NOT NULL,
  `activation_selector` varchar(255) DEFAULT NULL,
  `activation_code` varchar(255) DEFAULT NULL,
  `forgotten_password_selector` varchar(255) DEFAULT NULL,
  `forgotten_password_code` varchar(255) DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED DEFAULT NULL,
  `remember_selector` varchar(255) DEFAULT NULL,
  `remember_code` varchar(255) DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED DEFAULT NULL,
  `active` tinyint(1) UNSIGNED DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `middle_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `user_type` int(11) UNSIGNED NOT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` timestamp NULL DEFAULT NULL,
  `has_exam` tinyint(1) NOT NULL DEFAULT 0,
  `created_by` int(11) NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `email`, `activation_selector`, `activation_code`, `forgotten_password_selector`, `forgotten_password_code`, `forgotten_password_time`, `remember_selector`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `middle_name`, `last_name`, `company`, `phone`, `user_type`, `start_date`, `end_date`, `has_exam`, `created_by`, `batch`) VALUES
(1, '127.0.0.1', 'administrator', '$2y$10$bhNy.pp3yuSJYJbjc3VfR.iTo53fW38QTmB8OaH1dKMw8SCpb48Q6', 'admin.coe@gmail.com', NULL, '', NULL, NULL, NULL, NULL, NULL, 1268889823, 1576660087, 1, 'Juan', 'Dela', 'Cruz', 'PHP', '123', 1, NULL, '2019-08-07 15:01:08', 0, 1, 0),
(95, '::1', '2019_00002', '$2y$10$ho3WZOjr4P8KH9N9U0TeB.Go.DvuuPbQvphgvCZej2Vh7JN8eUMAW', '2019_00002', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1571748603, NULL, 1, 'Carl Louis', 'Vitug', 'Calma', NULL, '351088719', 3, NULL, NULL, 1, 1, 1),
(96, '::1', '2019_00096', '$2y$10$1qZxZGxPWAHx7afl.ZORfOdNQMZrFE83tV7i0X9Yz9G6XF6Iqm8.2', '2019_00096', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1571750975, NULL, 1, 'Juana', 'Barreto', 'Cruz', NULL, '351099817', 3, NULL, NULL, 0, 1, 1),
(297, '', '2019_00097', '$2y$10$QvndJ0JbAiQy8HWSNNE2yuZN5dRY/BCyHoLOxJXwZHvSBKthHeUl6', '2019_00097', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, 'Carl1', 'Louis1', 'Calma1', NULL, '351088719', 3, NULL, NULL, 0, 1, 1),
(298, '', '2019_00298', '$2y$10$sWR.URD7uvmjWgql88TNpeiDao9MEutHylz/Jif6cgJ3pWGpo8g/C', '2019_00298', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, 'Carl2', 'Louis2', 'Calma2', NULL, '351088720', 3, NULL, NULL, 1, 1, 1),
(299, '', '2019_00299', '$2y$10$QSkt2w8w5LVT/OeDcDkQ8epGBKTCdEWFYwI34TQ4wcQ/pktkCgRMC', '2019_00299', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, 'Carl3', 'Louis3', 'Calma3', NULL, '351088721', 3, NULL, NULL, 1, 1, 1),
(300, '', '2019_00300', '$2y$10$yJAlD0WOIbvkDBUuzI4ef.E9P/YBmK8zbU.elv8MikLdwJN07UjSC', '2019_00300', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, 'Carl4', 'Louis4', 'Calma4', NULL, '351088722', 3, NULL, NULL, 1, 1, 1),
(301, '', '2019_00301', '$2y$10$ewc//krLsigEHXYRFyWRLuXc3th0LWxVx8Ie81i/WYafSIpHIXf2i', '2019_00301', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, 'Carl5', 'Louis5', 'Calma5', NULL, '351088723', 3, NULL, NULL, 1, 1, 1),
(302, '', '2019_00302', '$2y$10$1ehWh5oxpiOZeaGf/1DPu.NUStAuAtdphB5i6MrhpmucMHlyjUhX6', '2019_00302', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, 'Carl6', 'Louis6', 'Calma6', NULL, '351088724', 3, NULL, NULL, 1, 1, 1),
(303, '', '2019_00303', '$2y$10$k7bcdhbT5FCUXSElKkQC3e9QSN8jDlZKZOpS/9z6uJJFnD8besB2q', '2019_00303', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, 'Carl7', 'Louis7', 'Calma7', NULL, '351088725', 3, NULL, NULL, 1, 1, 1),
(304, '', '2019_00304', '$2y$10$HZ4zfvzih8lkYDSOchaNFuaLhwMdLjkWTANR1JxBsGoXZtqhRuPXO', '2019_00304', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, 'Carl8', 'Louis8', 'Calma8', NULL, '351088726', 3, NULL, NULL, 1, 1, 1),
(305, '', '2019_00305', '$2y$10$2sWTlkogbHgqodCWv..rleUcJMQrmwdoPpiDgbR.me8xH5avuNo3u', '2019_00305', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, 'Carl9', 'Louis9', 'Calma9', NULL, '351088727', 3, NULL, NULL, 1, 1, 1),
(306, '', '2019_00306', '$2y$10$/Ta9pWl1BypEi3ns..XY4ux5f6UeLKAmNXek9/toxUDCouanjdyO.', '2019_00306', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, 'Carl10', 'Louis10', 'Calma10', NULL, '351088728', 3, NULL, NULL, 1, 1, 1),
(307, '', '2019_00307', '$2y$10$sy//fUlOJc/Zl4E8LXYa6uVrBaLgM59NBqtfxdj.Pf/MgYXfZejxi', '2019_00307', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, 'Carl11', 'Louis11', 'Calma11', NULL, '351088729', 3, NULL, NULL, 1, 1, 1),
(308, '', '2019_00308', '$2y$10$DHH6v.AWAmQnftXb02pLReGrsYpLP9UeUhVwWLAgQF0WiD5Nl81Em', '2019_00308', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, 'Carl12', 'Louis12', 'Calma12', NULL, '351088730', 3, NULL, NULL, 1, 1, 1),
(309, '', '2019_00309', '$2y$10$H8kF3VEKs69/IiGPlJAH0uLMmwdoMbUX6cWobf9MFU4QW2S6RNugW', '2019_00309', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, 'Carl13', 'Louis13', 'Calma13', NULL, '351088731', 3, NULL, NULL, 1, 1, 1),
(310, '', '2019_00310', '$2y$10$NM6yfkwbEb1JoBA9B7BXzOE894.VH5GHkrK3WyJiXLM0kj3XeuV5G', '2019_00310', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, 'Carl14', 'Louis14', 'Calma14', NULL, '351088732', 3, NULL, NULL, 1, 1, 1),
(311, '', '2019_00311', '$2y$10$aftUQiakxhaJDCda2VW3V.r5mDrqtoefqHd3fQ9fv29d1CLkM7U2C', '2019_00311', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, 'Carl15', 'Louis15', 'Calma15', NULL, '351088733', 3, NULL, NULL, 1, 1, 1),
(312, '', '2019_00312', '$2y$10$4TCtulGmnw4uQZsFnvn1Se9pFB/iH8gWwY5B565x3d5W1gQ3XCF.a', '2019_00312', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, 'Carl16', 'Louis16', 'Calma16', NULL, '351088734', 3, NULL, NULL, 1, 1, 1),
(313, '', '2019_00313', '$2y$10$afH6wARTClWpviE/j3DRs.o9RnpIetuzRMENA6kF/houA89aK1rZG', '2019_00313', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, 'Carl17', 'Louis17', 'Calma17', NULL, '351088735', 3, NULL, NULL, 1, 1, 1),
(314, '', '2019_00314', '$2y$10$Da.B0FNhuLfhwIZ4aPWY8uKzeplp8TVc915yFj49C0WxesEdc0BZ2', '2019_00314', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, 'Carl18', 'Louis18', 'Calma18', NULL, '351088736', 3, NULL, NULL, 1, 1, 1),
(315, '', '2019_00315', '$2y$10$xdvHRgG3ih/erQ3ywzgVNOFllojjnLSnQgGvdVpn2hq9T0z29xbNS', '2019_00315', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, 'Carl19', 'Louis19', 'Calma19', NULL, '351088737', 3, NULL, NULL, 1, 1, 1),
(316, '', '2019_00316', '$2y$10$dmgJcLpwt0N.CUyIr.5DVeyKYHF3aJ71sp2woMrCb4HxQDPGBPDQS', '2019_00316', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, 'Carl20', 'Louis20', 'Calma20', NULL, '351088738', 3, NULL, NULL, 1, 1, 1),
(317, '', '2019_00317', '$2y$10$wLIadPb6KSjgKihFA2eTwusWswA2uXCPeJsyqtlBCdr6jaqCP3Fq2', '2019_00317', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, 'Carl21', 'Louis21', 'Calma21', NULL, '351088739', 3, NULL, NULL, 1, 1, 1),
(318, '', '2019_00318', '$2y$10$t1Auk8fHixTypeCyfr9ZXuazQCbV3VKPHIQ20TAYL4D4vxpaY6hpu', '2019_00318', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, 'Carl22', 'Louis22', 'Calma22', NULL, '351088740', 3, NULL, NULL, 1, 1, 1),
(319, '', '2019_00319', '$2y$10$g4pkbXmpOc3w2mj4JP6uqe.XSzf74cdBPzZZSWIFz9FVoOd3KFtYO', '2019_00319', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, 'Carl23', 'Louis23', 'Calma23', NULL, '351088741', 3, NULL, NULL, 1, 1, 1),
(320, '', '2019_00320', '$2y$10$imvMFpwdJInd4OFXV03jUuZ.e//k5n0utPYpfP.VAZvoZ8dIuybbS', '2019_00320', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, 'Carl24', 'Louis24', 'Calma24', NULL, '351088742', 3, NULL, NULL, 1, 1, 1),
(321, '', '2019_00321', '$2y$10$uM32oPAJcdozofWrXYcupuvP2MfDeXadLWwPDYxl7k9ZYmhhcxq2y', '2019_00321', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, 'Carl25', 'Louis25', 'Calma25', NULL, '351088743', 3, NULL, NULL, 1, 1, 1),
(322, '', '2019_00322', '$2y$10$v20NXnhyr5tIVl8FGogerOaaSjHIFsqExg9Oaf4yHFMqQcc29todC', '2019_00322', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, 'Carl26', 'Louis26', 'Calma26', NULL, '351088744', 3, NULL, NULL, 1, 1, 1),
(323, '', '2019_00323', '$2y$10$b2jHtk44DHmkoJ5WvHhNSuPwqlqCJS7B9ssX9LU3Cg3CJmCTI8wYe', '2019_00323', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, 'Carl27', 'Louis27', 'Calma27', NULL, '351088745', 3, NULL, NULL, 1, 1, 1),
(324, '', '2019_00324', '$2y$10$0h3KrfyRVkMUKR3q63NM/uSB0o.GxYlj.4/mWOi4q7ccDK4kNR7ha', '2019_00324', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, 'Carl28', 'Louis28', 'Calma28', NULL, '351088746', 3, NULL, NULL, 1, 1, 1),
(325, '', '2019_00325', '$2y$10$Y/Mlsh89L5oHXHpoEecW6.FZou.jPvBOWFvCuWA.zelTag3Pinare', '2019_00325', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, 'Carl29', 'Louis29', 'Calma29', NULL, '351088747', 3, NULL, NULL, 1, 1, 1),
(326, '', '2019_00326', '$2y$10$WJtBm.UC2XcUkQhvsxfqZuwHk9nE9hZSDJV/9GGbWasfE9RVN0vA2', '2019_00326', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, 'Carl30', 'Louis30', 'Calma30', NULL, '351088748', 3, NULL, NULL, 1, 1, 1),
(327, '', '2019_00327', '$2y$10$qV0roNcZz0YTiihTMwpnJerHEWG8ksXpLQi6n4nkgW.DFyb.pIzSy', '2019_00327', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, 'Carl31', 'Louis31', 'Calma31', NULL, '351088749', 3, NULL, NULL, 1, 1, 1),
(328, '', '2019_00328', '$2y$10$mSHN52iBKbcbuj4oNuhq6ehH3nuegw9lAHzEPRu4NWvzEwjg40.3u', '2019_00328', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, 'Carl32', 'Louis32', 'Calma32', NULL, '351088750', 3, NULL, NULL, 1, 1, 1),
(329, '', '2019_00329', '$2y$10$TFL1bCheBC/sTYW5pHxL6usEt4L8imD0uhfTvjRl5j.744zzFeRAO', '2019_00329', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, 'Carl33', 'Louis33', 'Calma33', NULL, '351088751', 3, NULL, NULL, 1, 1, 1),
(330, '', '2019_00330', '$2y$10$wxAWAwzS35O19c.lSoEj0e9kNCcaLoqz6qeNTzdTLvbF8n902bKGi', '2019_00330', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, 'Carl34', 'Louis34', 'Calma34', NULL, '351088752', 3, NULL, NULL, 1, 1, 1),
(331, '', '2019_00331', '$2y$10$wVdIB2laxeuf/6KgeeL66O3ByRBYPk/VoX0MabMC1nN6l2ziZ65Bq', '2019_00331', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, 'Carl35', 'Louis35', 'Calma35', NULL, '351088753', 3, NULL, NULL, 1, 1, 1),
(332, '', '2019_00332', '$2y$10$8r/Vdkp3G1gUhk9/9Yi25uAVrGAKxHeC59opFGGGgtNq8xvQN.kUW', '2019_00332', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, 'Carl36', 'Louis36', 'Calma36', NULL, '351088754', 3, NULL, NULL, 1, 1, 1),
(333, '', '2019_00333', '$2y$10$FDaolVt20LEYMBo5ysqkY.Xzbl7xef/2BP9LQdvay2TYige.m5GGa', '2019_00333', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, 'Carl37', 'Louis37', 'Calma37', NULL, '351088755', 3, NULL, NULL, 1, 1, 1),
(334, '', '2019_00334', '$2y$10$RMkJTOV0YbOm0oTu1fpR.e7C17e/IMMg2U9/9935XPWfHfXpEGDoa', '2019_00334', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, 'Carl38', 'Louis38', 'Calma38', NULL, '351088756', 3, NULL, NULL, 1, 1, 1),
(335, '', '2019_00335', '$2y$10$I78dRKv2FkWLah15LLOF8uqV6zSnvxZMBKDw6mHH5wE7vEyKyRyX.', '2019_00335', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, 'Carl39', 'Louis39', 'Calma39', NULL, '351088757', 3, NULL, NULL, 1, 1, 1),
(336, '', '2019_00336', '$2y$10$RkojvfPiWinWe7WdcZuBcueOWatdqo5Pv06RObAMaWFgx6IDREqJa', '2019_00336', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, 'Carl40', 'Louis40', 'Calma40', NULL, '351088758', 3, NULL, NULL, 1, 1, 1),
(337, '', '2019_00337', '$2y$10$6I9QVvNLUwU2LlEcVab7leyveEz1Ww4b4b9yYVji1C9mZjH5VWhYa', '2019_00337', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, 'Carl41', 'Louis41', 'Calma41', NULL, '351088759', 3, NULL, NULL, 1, 1, 1),
(338, '', '2019_00338', '$2y$10$n2mzy906LTTEwlR0rlYGIOm9HWUFTVC08NcP81AaSHXJgY.G/N6A.', '2019_00338', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, 'Carl42', 'Louis42', 'Calma42', NULL, '351088760', 3, NULL, NULL, 1, 1, 1),
(339, '', '2019_00339', '$2y$10$wpy13OrkhcaBgp7XZigBxuegx7P99.Ar73MaVXAJK6nhv7mFj4sMe', '2019_00339', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, 'Carl43', 'Louis43', 'Calma43', NULL, '351088761', 3, NULL, NULL, 1, 1, 1),
(340, '', '2019_00340', '$2y$10$DH0sHFQutaym2HKweKEeCuXdmloIJpZvdhs8SQCRIcT9WduGjW70.', '2019_00340', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, 'Carl44', 'Louis44', 'Calma44', NULL, '351088762', 3, NULL, NULL, 1, 1, 1),
(341, '', '2019_00341', '$2y$10$lv.n8MzfFm8g9WnKy3Awaepa2e0zmME.zow.8Z9mLGbbUgCWKB5Zq', '2019_00341', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, 'Carl45', 'Louis45', 'Calma45', NULL, '351088763', 3, NULL, NULL, 1, 1, 1),
(342, '', '2019_00342', '$2y$10$VO07/cNrkaZBerTqwM9it.Zp4y7hEpvgdPDAegn4PbXhHusuh7S7S', '2019_00342', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, 'Carl46', 'Louis46', 'Calma46', NULL, '351088764', 3, NULL, NULL, 1, 1, 1),
(343, '', '2019_00343', '$2y$10$eff0yqfciaSaBGtvFOfNaeciYCgwEwByHdnzpklNYFQt9CpH8stg6', '2019_00343', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, 'Carl47', 'Louis47', 'Calma47', NULL, '351088765', 3, NULL, NULL, 1, 1, 1),
(344, '', '2019_00344', '$2y$10$WqqQlhTkOe48zFlVK7jYyOSGv573n84W1hF0w7q4AYqb7HmWb6IV.', '2019_00344', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, 'Carl48', 'Louis48', 'Calma48', NULL, '351088766', 3, NULL, NULL, 1, 1, 1),
(345, '', '2019_00345', '$2y$10$jrsPVJRS1F5VAIzr1iG3U.6IRa9bwm3byb5uDPFegOo3rAVSYmr6S', '2019_00345', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, 'Carl49', 'Louis49', 'Calma49', NULL, '351088767', 3, NULL, NULL, 1, 1, 1),
(346, '', '2019_00346', '$2y$10$47p72ieqsTTyXEFa52/0KOxAD3.wTHvr5VZKifURBXq9QUB6CCnqe', '2019_00346', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, 1, 'Carl50', 'Louis50', 'Calma50', NULL, '351088768', 3, NULL, NULL, 1, 1, 1),
(348, '::1', 'carlinvi02', '$2y$10$NDNIZlBZOu2PtolBGPwqDuxkFapRnPEVP3fbOZ0ZRvXr7verOmtXC', 'carlinvi02', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1571791156, 1571791166, 1, 'Carl', 'Invi', 'Ctus', NULL, '351088719', 2, NULL, NULL, 0, 1, 1),
(349, '::1', 'admin', '$2y$10$jboXTarCeU.g8jYf.nzQC.EwJA3l47vQcvmypru4uoG.zzw8y//zi', 'admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1571792510, NULL, 1, 'Admin', 'Admin', 'Admin', NULL, '123123111', 2, NULL, NULL, 0, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

CREATE TABLE `users_groups` (
  `id` int(11) UNSIGNED NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(68, 95, 2),
(69, 96, 2),
(70, 348, 2),
(71, 349, 2);

-- --------------------------------------------------------

--
-- Table structure for table `user_types`
--

CREATE TABLE `user_types` (
  `type_id` int(11) UNSIGNED NOT NULL,
  `name` varchar(55) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_types`
--

INSERT INTO `user_types` (`type_id`, `name`) VALUES
(1, 'SuperAdmin'),
(2, 'Admin'),
(3, 'Student');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `course_passed`
--
ALTER TABLE `course_passed`
  ADD PRIMARY KEY (`category_id`,`student_id`),
  ADD KEY `student_id` (`student_id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `log_history`
--
ALTER TABLE `log_history`
  ADD PRIMARY KEY (`history_id`);

--
-- Indexes for table `options`
--
ALTER TABLE `options`
  ADD PRIMARY KEY (`option_id`),
  ADD KEY `question_id` (`question_id`),
  ADD KEY `option_id` (`value`);

--
-- Indexes for table `questions`
--
ALTER TABLE `questions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `answer` (`answer`);

--
-- Indexes for table `student_results`
--
ALTER TABLE `student_results`
  ADD PRIMARY KEY (`student_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_email` (`email`),
  ADD UNIQUE KEY `uc_activation_selector` (`activation_selector`),
  ADD UNIQUE KEY `uc_forgotten_password_selector` (`forgotten_password_selector`),
  ADD UNIQUE KEY `uc_remember_selector` (`remember_selector`),
  ADD KEY `department_id` (`user_type`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  ADD KEY `fk_users_groups_users1_idx` (`user_id`),
  ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- Indexes for table `user_types`
--
ALTER TABLE `user_types`
  ADD PRIMARY KEY (`type_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `category_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `log_history`
--
ALTER TABLE `log_history`
  MODIFY `history_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=177;

--
-- AUTO_INCREMENT for table `options`
--
ALTER TABLE `options`
  MODIFY `option_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=121;

--
-- AUTO_INCREMENT for table `questions`
--
ALTER TABLE `questions`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;

--
-- AUTO_INCREMENT for table `student_results`
--
ALTER TABLE `student_results`
  MODIFY `student_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=347;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=350;

--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=72;

--
-- AUTO_INCREMENT for table `user_types`
--
ALTER TABLE `user_types`
  MODIFY `type_id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `options`
--
ALTER TABLE `options`
  ADD CONSTRAINT `options_ibfk_1` FOREIGN KEY (`question_id`) REFERENCES `questions` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Constraints for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
